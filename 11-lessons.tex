\chapter{Lessons learned and perspectives}\label{sec:lessons}

We start with the nicer parts of the long journey towards the present shape of the corpus \refS{sec:lessons:positive} and continue by a list of some pitfalls we fell into \refS{sec:lessons:negative}. Finally, we draw a sketch of how to proceed further \refS{sec:lessons:outlook}.

\section{What we would do the same way again}
\label{sec:lessons:positive}

\subsection*{The result is worth the trouble}

Experience from teaching Czech as a foreign language clearly indicates the need for a rich source of data on the language of learners, one which would help to design an intuitive presentation of the Czech language for non-native speakers, accompanied by exercises and tests. A learner corpus is the answer also because the typological properties of Czech as a highly inflectional language make the use of experience from other, better positioned languages at least questionable. In this sense, Czech may serve as a testbed for the development of methods and tools targeting inflectional languages.\footnote{Some aspects in the design and compilation of \corp{CroLTeC} \refS{sec:corpora:croltec} and \corp{RLC} \refS{sec:corpora:rlc} were influenced by \czesl as both a positive and a negative example. Foundations of a learner corpus of Polish, inspired by \corp{CzeSL in TEITOK} \refS{sec:data:teitok} are described by \citet{Kaczmarska:Zasina:2020}.}

\subsection*{Several approaches, various uses}

We have designed and implemented several approaches to the concept, collection, annotation and exploitation of a learner corpus of Czech, resulting in several releases, available for on-line queries and downloads. 
The corpus data have been found useful as a resource for building proofing tools and other NLP applications and, so far at least to some extent, for the practice of teaching Czech as a foreign language and research in its acquisition. Furthermore, some of the methods and tools developed within this project have been re-used in other projects.

\subsection*{Wide user focus reflected in the annotation}

A learner corpus may be intended for a group of users with specific research or practical needs,\footnote{For Czech, \corp{MERLIN} \refS{sec:corpora:merlin} may be an example of a learner corpus of this type, designed to provide texts illustrating the CEFR proficiency levels.} or for a wide audience of language acquisition experts, researchers or practitioners. \czesl has gone the latter path, necessitating some compromise or generalizing solutions which may not quite fit a specific goal of the user. The wider focus is  reflected especially in the approach to error annotation, which employs categories adopted from established grammar-based descriptions of native language rather than from a specific view of the learners' interlanguage. We believe that this type of annotation can serve as a common ground for uses of various kinds with the option to provide additional, perhaps more targeted annotation. Moreover, its compatibility with the annotation used in the native Czech parts of the \corp{CNC} supports comparisons of native and non-native Czech based on search results or statistics.

\subsection*{Several complementary types of error annotation}

Annotation in general, and error annotation in particular, takes up a substantial share of space in this book, because it represents some of the essential analysis steps of learner texts, essential in most types of corpus use. Recognizing the variety of users' expectations and the multitude of learner text aspects contending to be shown in the annotation, we have designed or modified several annotation schemes and error taxonomies. They are applied to complement each other rather than as alternatives.

The schemes differ also in how they resolve the frequent inherent impossibility to decide about the causes of observed deviations from the standard language. In the 2T scheme the annotator is instructed to pick the ``most sophisticated'' error tag \refS{sec:errors:tiers:manual:gram-form}. On the other hand, in the MD scheme, two or more tags related to different domains, such as spelling, morphonology, morphology or syntax, can be used simultaneously to provide alternative descriptions of an error if the cause is not clear. 


% It is no simple task to design an annotation scheme for a learner corpus and to maintain consistency in the annotated texts, both in a way that would reflect most demands of the corpus users. One of the main reasons is that annotating learner texts tends to be a highly specific enterprise, and even seemingly similar projects do not offer enough guidance – solutions are often too specific to a language or to the project concept and user requirements. On the other hand, annotation itself is quite rewarding due to the plentiful feedback about all aspects of the task and, of course, about the learners’ interlanguage.

% Our experience has confirmed that the design of the annotation scheme and the error taxonomy, as well as the choice of methods and tools has a profound effect on the final result. While the choice of two annotation tiers seems to be an optimal strategy, the error taxonomy could be modified in response to the experience from annotating larger volumes of data and the users' feedback.


\subsection*{Benefits of grammar-based error annotation}

\index{error!grammar-based}

The support of varied types of use is not the only reason why we opted for an annotation scheme and error taxonomy based on grammatical deviations from the standard, without a specific focus. Other reasons are due to concerns about annotation as a process. The grammar-based strategy fits well with the typological properties of Czech and the fairly common homonymy and synonymy of affixes. \index{homonymy} A grammar-based, \ie formally well-defined, taxonomy has a desirable effect of maintaining better consistency of manual annotation. At the same time, it allows for extensions into new domains of annotated phenomena, and into more efficient annotation processes, such as automatic assignment of more detailed error categories, automatic morphological and syntactic analysis or (semi-)automatic correction and error tagging. 


\subsection*{Automatic annotation}

\index{annotation!automatic}

Nearly all linguistic annotation tasks have been referred to taggers and parsers, even tasks involving source texts, for which the tools were not designed or trained. It turns out that a source learner text tagged and lemmatized this way is definitely more useful than the same text without any linguistic annotation, even if the bonus is merely the clear identification of non-words. 

For error annotation, proofing tools providing automatic corrections as well as purpose-built error identifiers were used in pre-processing and post-processing to assist annotators, or in fully automatic annotation of larger volumes of texts. While a combination of manual and automatic annotation is appreciated by the annotators and corpus users, results of a fully automatic process are considerably less reliable and sophisticated than manual error annotation, but still useful for some purposes, as evidenced by research based on the automatically annotated \corp{CzeSL-SGT} corpus (\eg \cite{Hudouskova:2013,Hudouskova:2014,Novak:etal:2017,Novak:etal:2019}) and the history of user interactions with this corpus via the search interface. A fully automatic annotation is obviously justified as an alternative to manual annotation when the demand for large data is higher than concerns about the error rate.


%As a rule-based system it is not easily modifiable to another language, but the approach to error annotation of learner language according to the levels of language description with the possibility of assigning multiple potential error labels to one error is, in our opinion, useful for error annotation of learner's texts in any language.



\subsection*{The importance of annotators' feedback }

The annotation process brings plentiful feedback, reflected in discussions in the web forum, training sessions for the annotators and in the annotation manual. The feedback helped to improve instructions to deal with thorny issues such as the uncertainty about the author’s intended meaning and its expression, the inference errors, the proper amount of interference with the original, or the occurrence of colloquial language. In all of this, annotators should handle similar phenomena in the same way.

The IAA results show that the rules for manual tagging of errors in spelling, morphonology, morphology and morphosyntax, such as \tg{incorStem}, \tg{incorInfl}, \tg{agr} and \tg{dep}, are assigned fairly consistently. However, we were unable to obtain a similarly robust annotation of semantic errors, which are much more dependent on subjective judgment. It is even unclear whether it is desirable to aim at a standard for their annotation.


\section{Blind alleys and second thoughts}
\label{sec:lessons:negative}

\subsection*{Sloppy planning}

In order to reach its goals and become useful, a learner corpus project should be conceived carefully, considering many factors and avoiding blind spots in the plan, from text collection to user access. A change in the corpus design may leave permanent traces. For example, \textit{CzeSL-plain} and its hand-annotated part \textit{CzeSL-man v0\/} include a substantial share of the Romani ethnolect, actually produced by native speakers of a dialect of Czech, rather than by non-native speakers of Czech. This is due to the original strategy of grouping texts by the way they are processed. This has been changed in later releases, where texts produced by non-native and native learners (the latter including speakers of the Romani ethnolect of Czech) are parts of distinct corpora.

Another example is the fact that many texts have not been processed properly. In the early days of the project, most efforts were focused on collecting, transcribing and hand-annotating, yet not all transcribed texts were annotated and checked, and no doubly annotated texts were adjudicated, as originally planned. Later, resources for such tasks dried up, which invited more interest in automating the annotation tasks.

\subsection*{Missing or inaccurate metadata} \label{sec:mising-meta}

\index{metadata}
Neither \textit{CzeSL-plain} nor \textit{CzeSL-man v0} include the full set of metadata, which were not available in the appropriate form and content at the time the two corpora were prepared and released. In \textit{CzeSL-plain}, the texts are categorized into three groups: as essays, written either by non-native learners, or by speakers of the Roma ethnolect of Czech, and as theses written by non-native students. In the searchable release of \textit{CzeSL-man v0}, even this basic distinction is not available. 

An issue of a different sort is the unreliability of an important metadata item. The CEFR level is specified according to the teacher's assessment, because many texts in \czesl do not come from test situations. As a result, the information about proficiency level is a major weakness of the corpus. The CEFR classification of texts is based on a holistic evaluation made by teachers, collectors or annotators, or according to the level of the whole class.
Collectors and annotators were instructed and trained about the proficiency levels, using guidelines and references. Many of them were experienced teachers of Czech as a foreign language. 

Yet even experienced educators and methodologists may arrive at different ratings for a single learner, while learners in a single class can differ substantially in their levels. Moreover, the CEFR level evaluation standard for Czech was not available at the time the texts were collected, transcribed and annotated. Without an independent objective metrics it was difficult to provide a consistent classification, especially across various L1s.  

Ideally, the texts should be re-evaluated, as was done, for example, in the \corp{MERLIN} project, but it would be a labor-intensive project in itself, if done manually.



\subsection*{Multiple taggers and the multidimensional tagset}
\index{error!categorization}


We did not pursue all ideas about automatic annotation until their implementation in the annotation toolchain. One of the most interesting experiments was an attempt to apply different POS tagging methods to the source text, as in \citet{Diaz:etal:09}. We expected different results for faulty forms across the taggers and planned to implement a method proposing a hypothesis about the error type by comparing these results. However, the results of multiple taggers, based on different tagging strategies, lead to a usable interpretations of faulty forms only in a limited number of cases. 

We also tried to apply the concept of multidimensional word classes, at first in combination with the application of multiple taggers, and later -- independently from any approach to the annotation process -- as a categorization of morphological and morphosyntactic errors in Czech. According to this initial and later abandoned blueprint, 
the dimensions coincided with the lexical, morphological and syntactic properties of a word form. 
As in \citet{Diaz:etal:09}, the assumption was that phenomena of non-standard language can be modelled as mismatches between the three dimensions. 
For example, in \ee{Petr viděl *\textbf{lev}}{Petr saw a lion} instead of \e{Petr viděl \textbf{lva}}, 
\ee{lev}{lion} is morphologically nominative, but syntactically accusative (\ee{viděl}{saw} requires its object to be in the accusative case). 
%
In \ee{Eva *\eb{bude} *\textbf{napsat} dopis}{Eva will write a letter} instead of \e{Eva \textbf{napíše} dopis} or \e{Eva \eb{bude} \textbf{psát} dopis}, 
the ‘lexical’ aspect of the content verb \e{napsat} is perfective, 
while the auxiliary verb \e{bude} has a ‘syntactic’ requirement for an imperfective form \textbf{\e{psát}}. 
%
In \ee{Whitney Houston zpívala *\textbf{krásný}}{Whitney Houston sang beautiful} instead of \e{Whitney Houston zpívala \textbf{krásně}}, 
the author used an adjective \ee{krásný}{beautiful} rather than the adverbial \ee{krásně}{beautifully}. 
The word can be annotated as an adjective in the morphological dimension and as an adverb in the syntactic dimension.

However, the morphological idiosyncrasies of non-native Czech call for additional error categories capturing morphemic rather than morphosyntactic phenomena. 
The wrong form \e{leva} in \refp{ex:error:md:leva} could still be modelled as a mismatch across the different dimensions of the annotation, 
in this case between the proper ``lexical'' paradigm and the ``inflectional'' paradigm assumed by the learner.
Mismatches between different aspects of the analysis of a form should then coincide with a taxonomy of errors.

Despite its theoretical appeal and an affinity with standard
linguistic concepts, this approach to classifying learner errors in
Czech morphology turned out to be imposing a somewhat artificial
paradigm upon the empirical facts.

The crucial difference between the original concept of multidimensional word classes and the final design of the multidimensional error annotation scheme \refS{sec:error:md} is in the interpretation of multidimensionality. According to the original proposal, an error was characterized by the conjunction of  dimensions. According to the final design, the dimensions (or linguistic domains) are treated as alternative explanations of the error, \ie in disjunction. 


\subsection*{Unbalanced representation of text types and learner categories}

Some balance or at least representative proportions of text types (argumentative essays, descriptions) and learner categories (L1, CEFR level) are necessary or at least  useful. Tables~\ref{tbl:data:sgt:cefr-l1g}--\ref{tbl:doubly-group} show an opposite, opportunistic approach, driven by practical constraints,
often justified by the unavailability of texts of a specific category. To some extent, the imbalance has been remedied in more recent text collection and annotation rounds (see \refs{sec:data:th} and \refs{sec:data:teitok}).


\subsection*{Transcription}

\index{transcription}

To avoid the need of cleaning transcripts with improperly used mark-up, an editing tool including strict format controls is preferable to a free-text editor. 

\subsection*{Tokenization}

\index{token}

Designing tokenization rules specific to learner texts and different from those used in tools for native texts is not worth it. 
It makes it much harder to use existing NLP tools as they typically assume certain tokenization.


\subsection*{Annotation scheme vs.\ the ease of using the corpus}
\label{sec:scheme-use}

\index{query interface}
A scheme ideally suited to the data may turn into a problem later, if the consequences for the annotation process and the use of the corpus are not foreseen. Standard concordancers may require substantial tweaking of the data, while a custom-built tool may lack features of the tools developed for a long time. At the same time, most users of this type of corpora definitely need a friendly interface.

The 2T annotation scheme, designed to fit the needs of error annotation of L2 Czech, requires a specific corpus search tool or lossy conversion into a more common format. \textit{SeLaQ}, as the custom-built search tool, is able to process and search the 2T data without conversion and information loss, but cannot display the tiers in parallel, ignores metadata and lacks many features of a more mature corpus search tool.
On the other hand, the \tool{KonText} and \tool{TEITOK} search tools, even though they require conversion of the 2T format without retaining all details of the error annotation, offer many more user options. In fact, the \corp{CzeSL-man v2} and \corp{CzeSL in TEITOK} corpora go a long way towards handling most of the 2T error annotation, even if some
of the properties and information present in the 2T scheme get lost in the conversion to the format used by the corpus search tool.

\subsection*{Too many data formats}
\label{sec:too-many-formats}

\index{format}
The \czesl texts are available in several annotation schemes and various data formats. Formats of the on-line searchable releases correspond to the search tools and search interfaces while formats of the downloadable data sets to standards or preferences of potential users or applications. Some of these formats exist due to purely pragmatic reasons. As explained above in \nameref{sec:scheme-use}, a format used for transcription and annotation may not be suitable for searching using a standard tool with necessary features and must be converted to a different format. This is the case of the \corp{CzeSL-man v0} corpus, which has been converted to  \corp{CzeSL-man v1} and \corp{CzeSL-man v2}. The format of \corp{CzeSL-plain} and \corp{CzeSL-SGT} corpus, which were not annotated manually, was determined by the annotation tools and the search tool. 

Other formats were introduced to allow for annotation which was not originally previewed.
This is the case of the 2T annotation scheme. Together with the annotation editor it was designed to suit the error annotation of Czech with a focus on syntax and morphosyntax. The scheme was not meant to handle morphs and other segments smaller than a word. Also it was not intended to accommodate linguistic annotation of syntactic structure. These are the reasons why the formats used in the \corp{CzeSL-MD} and \corp{CzeSL-UD} corpora are different from the format used in the \corp{CzeSL-man v0} corpus.

A format suitable for a search tool is usually less flexible than the annotation format. The 2T annotation cannot be represented in the vertical format, used by the standard corpus search tools, without some loss of information. The loss is higher in  \corp{CzeSL-man v1}, representing only the TH and error tags at T2, and the corresponding tokens at T0, unless cross-tier links other than 1:1 are involved. The rest is discarded, including source tokens which cannot be associated 1:1 with T2 tokens. In \corp{CzeSL-man v2}, the loss is much lower: only corrections involving long-distance word order changes and some complex multiple cross-tier links are lost. The format used in \corp{CzeSL-MD} is even harder to reconcile with the token-based vertical format, because segments smaller than words can be annotated.

As a general solution, for multi-tier learner corpora annotated in various independent or interacting ways, a consistently applied stand-off annotation format is -- at least in theory -- the best solution.\footnote{The \tool{ANNIS} search tool site at \url{https://corpus-tools.org/annis/} includes also suggestions for annotation editors.} Our approach is similar. In order to integrate several corpus releases including the same texts annotated in different ways in different formats for the purpose of representing and searching the annotation in a consistent and user-friendly way, we convert existing non-token-based annotation into stand-off annotation applied to the texts. However, these texts are tokenized and any annotation which can be token based, is converted from the various sources and represented as token attributes. The annotation follows the TEI guidelines and the result is searchable by \tool{TEITOK}. 

There are several reasons behind our choice. The solution allows for compatibility with the standard token-based search tools: the same corpus can be searched in one tool and the corcordance represented in another tool. Moreover, the corpus, including its metadata, can be extended, modified and annotated within the same tool. The texts can also be viewed as facsimiles or at different annotation tiers. On the other hand, even though the conversions from various annotation formats can reveal some inconsistencies, they also require some human assistance and are not completely error-free. Obviously, a format and tools compatible with the desirable annotation and search options would be the ideal solution.



% the problem of too many different formats and browsing interfaces. Can all those formats be converted into each other? Any solid conclusions for future learner corpora projects? Any preferences?


\section{Outlook}
\label{sec:lessons:outlook}


% In addition to using automatic tools for extending manual annotation and providing basic markup for learner texts without manual annotation, our plans include a parser to assign syntactic annotation (functions and structure).\footnote{\citet{Ott:Ziai:2010} report that in texts produced by learners of German the main functor-argument relation types can generally be identified with precision and recall in the area of 80–90\%. This is an encouraging result, but the success will necessarily depend on the proficiency level of the learners.}

\begin{itemize}
\item Compilation of a balanced hand-annotated subset of \czesl
\item Integration of all \czesl texts with all available annotation in a single search and editing tool (\tool{TEITOK})
\item Periodical release of the updated corpus in \tool{KonText} 
\item Incremental inclusion of new texts, including annotation
\item Continuous proofreading of annotated content
\item Development of automatic tools for linguistic and error annotation of learner language
\item Manual annotation of learner texts to serve as training data for the tools
\end{itemize}



