\subsection{Morphemic analysis}
\label{sec:process:md:morphemic}

\index{annotation!automatic}
\index{annotation!morphs}
% \tomas{Následující by se dalo použít zde (jako jakýsi úvod v odstavci věnovaném automatickému preprocessingu.)}  
% \sasa{přesunuto}
The process of error classification, both manual and automatic, can be
simplified by a preliminary automatic rule-based analysis, which can add useful information about each error. It can
determine whether an error meets the criteria for an orthographic
error or distinguish between errors in word stems from
errors in inflectional affixes.
The input to the automatic identification of errors is the source
text and the final target hypothesis (TH), aligned word-to-word. We use
the manual corrections from \corp{CzeSL-man} for now, but we expect to use
automatically corrected word forms in the future.



All identified inflectable words%
\footnote{Inflectable words are identified according to the morphological tag automatically assigned to the word on T2 --
every word tagged on T2 as a noun, adjective, pronoun, numeral, verb or a graded adverb.} 
in the source texts (\ie all identified inflectable words at T0) undergo a simple morphemic analysis.
The analysis is simple in the sense that its ambition is not to divide the whole word into individual morphs, %\sasa{pořád někde čtu, že s pojmem morfém je problém, neměli bysme všude používat morf? nebo aspoˇň říct, že to tak myslíme}\tomas{Pro mě za mě, upravuju tedy na "morph".}
but only to mark inflectional prefixes and suffixes.
For example, for the form \eec{po$|$běž$|$í}{run}{fut},\footnote{%
We use \e{$|$} to mark relevant morpheme boundaries.} 
we mark the prefix \e{po-} and the suffix \e{-í}.\footnote{Here, \e{po-} is inflectional because it marks future tense of the verb \ee{běžet}{run};
unlike in the verb \ee{pomoci}{help}, where the prefix is derivational and is present in all forms of the verb. The suffix \e{í} expresses the 3rd person singular and plural for this verb.}
For verb participles, we mark both the participle suffix and the ending expressing gender and number:
in \eec{připravi$|$l$|$i}{prepared}{masc.pl} \e{-l-} is the past participle suffix and \e{-i} is the plural masculine animate ending.


The procedure compares the source words on T0 and the corresponding words on T2.
When the T0 word is correct, \ie the T0 and T2 words are equal, we assume that they have the same morphological properties,
and we use the morphological tag from T2 and segment the T0 word according to the T2 word.
When the T0 is corrected, \ie the T0 and T2 words are different, the situation is more complicated.

The automatic morphemic analysis proceeds as follows:
\begin{enumerate}
  \item If the source form is not corrected (\ie T0 = T2),
    we determine the inflectional suffix from the lemma and the tag assigned to the form, according to the rules of Czech inflection.
    The form \eec{ledničce}{refrigerator}{dat} with the lemma \e{lednička} and the tag \tg{NNFS6} (feminine noun in local singular) is analyzed as \e{lednič$|$ce}, based on its lemma and the phonetic alternation \et{k}{c} in the local case of feminine singular.

  \item If the source form on T0 is corrected on T2,
    but it is still an existing word (as determined by the morphological analysis) with the T2 lemma among its possible lemmas, the interpretation and therefore its morphemic analysis is based on the lemma of the corrected T2 form and the morphological tag of the source form.
    For example, the form \e{je} can be either the 3rd person singular present tense of the verb \ee{být}{be}, or the accusative plural of the pronoun \ee{on}{he}.
    In \refp{ex:process:quality:vanoce}, \e{je} is corrected as \ee{jsou}{are} and is therefore analyzed as the verb \ee{j$|$e}{is} (\ee{Vánoce}{Christmas} is a plurale tantum).

\begin{exe}
\ex\label{ex:process:quality:vanoce}
    \ttrexample
        {Vánoce \e{*}\textbf{je} nejdůležitější svátek.}
        {Christmas  is  most-important holiday}
        {Vánoce \textbf{jsou} nejdůležitější svátek.}
        {Christmas  are  most-important holiday}
        {Christmas is the most important holiday.}
        {HRD\_UE\_347 ru A2}
\end{exe}

  \item If the source form is not an existing (orthographically correct) Czech word,%
    \footnote{In that case, it is assigned the morphological tag “unknown word” and a lemma identical to its form.}
    we attempt to guess its correct morphemic analysis based on the comparison with its correction.
    We perform the morphemic analysis of the word on T2 and compare its stem and inflectional affix with the word form on T0.

  \item If the beginning of the source form matches the stem of the correction, we mark what follows as the source word's inflectional suffix:
    \eetc{měsíc$|$y}{měsíc$|$e}{month}{pl.acc}, \eet{lázn$|$e}{lázn$|$ě}{baths/spa}, \eetc{zasp$|$ám}{zasp$|$ím}{oversleep}{fut.1sg}, \eetc{jmenuj$|$em}{jmenuj$|$i}{name}{1sg}.

  \item If the source form ends with the characters of the corrected suffix and what precedes is similar to the corrected stem
    we assume that the source form has the same suffix as its correction:
    \eet{poměnk$|$a}{pomněnk$|$a}{forget-me-not}, \eet{vopic$|$e}{opic$|$e}{monkey}, \eet{vyjadři$|$t}{vyjádři$|$t}{express}, \eet{glavn$|$í}{hlavn$|$í}{main}.


  \item If the source form and its correction differ in both their beginnings and ends,
    but their beginnings are similar enough and the stem-final consonant from T2 can also be found at a similar position on T0,
    we assume that the stem boundary follows this consonant on T0 as well:
    \eetc{spiv$|$aji}{zpív$|$á}{sing}{3sg}, \eet{cistejš$|$i}{čistějš$|$í}{cleaner}, \eetc{vlaštn$|$ou}{vlastn$|$í}{own}{adj}.
    If the number of differences is small, we also assume a partial match between the stem-final consonants (\eg ignoring diacritics)
    \eetc{kultůr$|$u}{kultuř$|$e}{culture}{dat/loc}.


  \item In all other cases, morphemic analysis of the form is skipped.

\end{enumerate}
