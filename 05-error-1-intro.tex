% \subsection{The feasible and the desirable}
% \section{Things to consider in the design of an annotation scheme}
\section{A wishlist for error annotation}
\label{sec:error:general}

% \sasa{Dřív bylo: Things to consider in the design of an annotation scheme. Máte lepší nápad?}

Designing an error annotation scheme for non-native Czech is a
challenging task.  Czech, at least in comparison to most languages of
the existing annotated learner corpora, has a more complex morphology
and a less rigid word order, which opens annotation issues that had
not been addressed before the error annotation of \czesl started.  As
can be expected, the language of a learner of Czech may deviate from
the standard in a number of aspects: spelling, morphology,
morphosyntax, semantics, pragmatics or style.  To cope with the
multi-level options of erring in Czech and to satisfy the goals of the
project, the annotation scheme should:

\begin{enumerate}
\item Properly handle Czech as an inflectional and free-word-order language, 
    \eg support successive corrections and annotation of errors in discontinuous expressions
    
\item Be detailed and informative but manageable for the annotators,
    \eg preserve the original text alongside with its corrected version and
    represent syntactic relations for errors in agreement, valency, pronominal reference
  \item Be open to future extensions, allowing for alternative/more
    detailed taxonomy to be added later
  \item Provide solutions for issues of interference
    \refS{sec:error:general:interference}, interpretation
    \refS{sec:error:general:interpretation}, word order
    \refS{sec:error:general:wo} and style
    \refS{sec:error:general:style}
\end{enumerate}

\noindent
The resulting annotation scheme and the error typology is a compromise
between the limitations of the annotation process and the demands of
research into learner corpora.


\subsection{Interference and other types of explanation}
\label{sec:error:general:interference}

Interference figures prominantly among the candidates for the most relevant explanation of an error.  \index{interference} Interference (also called positive or negative language transfer, or crosslinguistic influence) involves an inappropriate use of linguistic features from another language known to the learner, usually their native tongue, or the
inappropriate avoidance of such features.

A sentence such as \ee{Tokio je pěkný hrad}{Tokio is a nice castle} is
grammatically correct, but its author, a native speaker of Russian,
was misled by ``false friends'', assuming \ee{hrad}{castle} as the
Czech equivalent of Russian \ee{gorod}{town, city}. \index{false friends} Similarly in
\ee{Je tam hodně sklepů}{There are many cellars}.  The formally
correct sentence may strike the reader as implausible in the
context. The interpretation becomes clear only with the knowledge that
\e{sklep} in Polish means `shop', not `cellar' (\ie \e{sklep} in Czech).

However, to identify and correct the error without more or less
thorough knowledge of the other language is impossible. In practical
terms, the identification of all types of interference in a corpus
with many L1s is very hard. Most of our annotators were no experts in
Czech as a foreign language or in L2 learning and acquisition, and
unaware of possible interferences between languages the learner
knows. Thus they would have very likely failed to recognize an interferential error. \index{error!interferential}

Interference is just one of many types of error diagnostics which is
different from grammar-based annotation or other relatively
straightforward categorization. The perspective subsuming
interference is concerned with the discovery of causes or
explanations. Apart from the practical issue of annotating such
properties without researching other resources, such as additional
texts from the same learner, perhaps at different stages of the
acquisition of L2, there is also a theoretical reason why the
explanation of errors should be kept separate from the more
down-to-earth types of linguistic annotation. Even though all
annotation is interpretation, interpretation in terms of grammar-based
categories or even stylistic appropriteness is governed by
instructions, linguistic rules and/or relations to L1, while
finding an explanation for an error can hardly be guided by
guidelines.

For such reasons, instead of its explicit annotation, interference and
error explanations of other types are assumed to be identified by the
corpus user in the process of interpreting the corpus data, other
types of annotation and the metadata.

%\footnote{All examples are authentic.}


\subsection{Interpretation in terms of TH}
\label{sec:error:general:interpretation}

For some types of errors, the problem is to define the limits of
interpretation in terms of TH.  \index{target hypothesis} \index{interpretation} Example \refp{ex:error:general:zlobna}
shows two possible interpretations (TH1 and TH2) of a grammatically
incorrect clause (S), corresponding to the concepts of ``minimal'' and
``maximal'' TH in the \corp{Falko} corpus
\refS{sec:corpora:falko}. The clause is roughly understandable as its
TH1 version, but it can also be rewritten as TH2, which is further
from the source clause. The TH1 version is less natural but closer to the original. However, to provide annotation in terms of TH2 the task
of the annotator is interpretation rather than correction.
%\jirka{slovosled v "citila - na tebe" by se nemel menit - wo se opravuje, jen kdyz ne negramaticky, jak piseme o par vet nize}, or \ee{kdyby se na tebe zlobila}{if she were angry at you};


\begin{exe}
\ex\label{ex:error:general:zlobna}
\begin{xlist}[TH2:]
\item[S:] \e{kdyby} *\e{c\eb{i}tila} \e{na} \e{tebe} *\e{\eb{zlobna}}
\item[TH1:] \gll kdyby \eb{se} c\eb{í}tila na tebe \eb{rozzlobená} \\
            if \catt{refl} felt at you angry\\
            \glt `if she felt angry at you'
\item[TH2:] \gll kdyby \eb{se} na tebe \eb{zlobila} \\
            if \catt{refl} at you {was-angry}\\
            \glt `if she was angry at you'
\end{xlist}
\end{exe}

% Daniela myslela, že to je urážka, a byla trochu zlobná.  HAJ_B2_004, czesl-plain, ciz; jiná metadata nejsou

\noindent
Without the option to provide both THs, as in \corp{Falko}, it is
difficult to provide clear guidelines. In the manual annotation of
\czesl, the TH is not supposed to aim at perfect Czech.  Instead, the
source text is corrected conservatively to arrive at a coherent and
well-formed result, without any ambition to produce a stylistically
optimal solution, refraining from too loose interpretation. In this sense, the annotator is instructed to minimize interpretation. In general, the ultimate TH in \czesl is closer to \corp{Falko}'s TH1 rather than TH2, unless the grammatically correct version is hard to understand or very unnatural.\footnote{For a related discussion about what counts as an error in L2 see \refs{sec:errors-learn-lang}.}
Where a part of the input is not comprehensible, it is marked as such and left without correction.



\subsection{Word order}
\label{sec:error:general:wo}

% \jirka{Pokud povazujeme rozdil v inf.strukture / tfa za veci gramatiky, tak to musime explicitne zminit. Standardni (necesky) pohled je podle me to, ze je to pragmatika. Ale radsi bych se tomu vyhnul a proste rekl, kdy ten slovosled opravujeme a kdy ne. Mne to z tohohle neni jasny.}

% \sasa{OK, asi teda říct, že opravujeme, když slovosled mění význam? Lepší takhle? Viz druhý odstavec.}

Czech constituent order reflects information structure \refS{sec:cze:wo} and it is sometimes difficult to decide (even in a context) whether an error is present.\footnote{See \refs{sec:errors-learn-lang} for more about ``covert errors''.} 
\index{error!word order}
\index{word order}
\index{information structure}
The sentence \ee{rádio je taky na skříni}{a radio is also on the wardrobe} suggests that there are at least two radios in the room, although the more likely interpretation is that among other things which happen to sit on the wardrobe, there is also a radio.
The latter interpretation requires a different word order: \e{na skříni je taky rádio}.

In accordance with the preference of conservative target hypotheses \refS{sec:error:general:interpretation}, word order should be corrected only when it is perceived as ungrammatical. Misplaced 2nd position clitics are a typical example, as in \eet{rozhodli \eb{se jsme}}{rozhodli \eb{jsme se}}{we have decided}. \index{clitic} However, in cases when word order (i) makes a difference in meaning, as in the switched order of the two NPs above (`the radio' and `the wardrobe'), and (ii) the context makes it clear which meaning is appropriate, word order should be corrected even though it is grammatical. In this sense,  word order and lexical corrections share the same approach: correction is due whenever an item or pattern does not fit the meaning of the context.

% However, in accordance with the preference of conservative target hypotheses \refS{sec:error:general:interpretation}, word order is corrected only when it is perceived as ungrammatical. Misplaced 2nd position clitics are a typical example, as in \eet{rozhodli \eb{se jsme}}{rozhodli \eb{jsme se}}{we have decided}.

% Similarly difficult may be decisions about errors labeled as \textbf{lexical} and \textbf{modality}.
% All such error types call for a more precise definition.



\subsection{Style} 
\label{sec:error:general:style}

The phenomenon of Czech diglossia \index{diglossia} (see Appendix \refs{sec:cze}) is reflected in the problem of annotating non-standard language, 
usually individual forms with colloquial morphological endings.
Because learners may not be aware of the status of these forms and/or an appropriate context for their use, \index{Colloquial Czech} Colloquial Czech (CCz) is corrected under the rationale that the authors expect the register of their text to be perceived as unmarked.  

To give a prototypical example, one of the most frequent problems in learner texts is the absence of appropriate diacritics. At the same time, a missing acute accents on some verbal endings in written text is perceived as colloquial, because it is supposed to reflect the colloquial pronunciation of these forms: \eet{znam}{znám}{I know}, \eet{nosim}{nosím}{I wear}. \index{error!diacritics} There are nearly 2.5 thousand instances of 180 different apparently colloquial verbs in the 1st person singular in the 1 million \corp{CzeSL-SGT} corpus. Cases like this are treated as errors (in spelling or morphonology), but they are also labeled as colloquial style, suggesting that the learner could have used a colloquial instead of an incorrect form.\footnote{The colloquial marker is in fact a category from the domain of linguistic rather than error annotation. However, it is used in the manual error annotation to make the point that some forms annotated as incorrect can also be interpreted as colloquial forms. The colloquial marker can be confirmed in the annotation provided by a tagger applied to the source text, although the automatic linguistic annotation of such forms may be less reliable then of their SCz counterparts.} It is up to the user of the corpus to interpret the annotation according to a wider context or the learner's profile in the metadata.

\subsection{Communication goal}
\label{sec:error:general:summaryr}

%%\bara{Summary mi přijde lehce matoucí. Vision?}
%%\sasa{Nebo rovnou Communication goal? I když ten závěr je o všem}
% \sasa{ještě takový scifi kandidát na anotaci: achievement of communicative goal, pamatuješ na Uppsalu? Jirka: SOuhlas aspon bych zminit, ze to bohuzel z praktickych duvodu nedelame, i kdyz samozrejme z urciteho pohledu muze byt na znalosti ciziho jazyka to nejdulezitejsi}
% \sasa{Takhle ok?}

Other features of the learner language may also be considered as candidates for annotation, such as a measure estimating to what extent the learner's communication goal is achieved. In fact, there is hardly anything that matters more in practice and could be reflected even at the level of individual utterances.

On the other hand, not every aspect of the learner language must be explicitly annotated. It could even be a more proper move to leave some of the trickier phenomena such as interference or exhaustive interpretation for the corpus user while providing a reliable annotation of errors where a safer ground is available in linguistic theory, established categories and the annotators' competence. Such annotation, provided ideally by the combination of the three annotation schemes, can help the user to interpret the search results or statistical findings in ways not previewed in the annotation.

