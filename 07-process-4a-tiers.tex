\section{Tiered error annotation}
\label{sec:process:tiers}

\todo{describe here or somewhere morphology annotation - see OLD}

\sasa{co je to OLD?}

The tiered error annotation proceeds in the following steps:

\begin{enumerate}
    \item Preprocessing:
        The transcript is converted into a format where
        T0 roughly corresponds to the tokenized transcript and T1 is set as equal to T0 by default.
        Both are encoded in PML, an XML-based format
        \refS{sec:data:xml}. \index{Prague Markup Language}
        The conversion includes basic checks for incorrect or suspicious transcription.


    \item Manual error annotation:
        Errors in the text are manually corrected and tagged; each annotation is checked by a supervisor \refS{sec:process:tiers:manual}.
        Some texts are independently annotated twice (see \refs{sec:error:tiers:quality}).

    \item Automatic annotation checks:
        Manually annotated texts pass through a series of automatic checks. Suspicious annotations are marked and manually reviewed.
        
    \item Manual adjudication:
        Each doubly annotated text should be checked and adjudicated, resulting in a single annotated version.
        However, except for a small pilot, this has not been done yet, so a part of the corpus actually contains two independent annotations. 

    % todo tohle mozna patri do ling anotace
    \item Linguistic (morphological) annotation:
        Target hypothesis is automatically annotated with lemmas and
        morphological tags, both full hypothesis on T2 \refS{sec:ling:stdcz:t2} and individual words on T1 \refS{sec:ling:stdcz:t1}.
        %\sasa{Uved bych tenhle postup jako postup anotace, ne jenom chybový anotace.}

    \item Automatic error annotation:
        %Error information that can be inferred automatically is added.    % \cite{Stindlova:etal:2013}
        Error information that can be inferred automatically is added by comparing original and emended words:
        type of spelling alternation, missing/redundant expressions, and inappropriate word order (see \refs{sec:error:tiers:formal}).
        
%       \sasa{Taky podrobnější klasifikace vbx atd.?}

\end{enumerate}

\noindent
Conversion to PML \refS{sec:data:xml}, annotation, supervision and adjudication are done with the help of \emph{feat},
an annotation editor designed as a part of the project \refS{sec:tools:feat}.
The storage of the documents and their flow within this process is managed by \emph{Speed},
a purpose-built text management system \refS{sec:tools:speed}.


\subsection{Manual error annotation}
\label{sec:process:tiers:manual}

\index{annotation!manual}

Some of the transcribed texts are error-annotated manually according to the 2T scheme described in \refs{sec:error:tiers}.
%
The annotation was done in \tool{feat} \refS{sec:tools:feat}.
The annotator corrected the text on appropriate tiers, modified relations between elements (by default all relations are 1:1) and annotated relations with error tags as needed.
\autoref{fig:process:tiers:feat} shows the annotation of a sample sentence as displayed by the tool.
The top of the window shows the currently annotated part of the sentence, displaying the source text above the two annotation tiers. The context of the annotated text is shown both as a transcribed HTML document (bottom left of the window) and as a scan of the original document (bottom right).

In the annotated part of the window, forms identified by the tool as non-words are underlined, corrections done by the annotators are in red. Unless the annotator decides otherwise, vertical links align the words across the tiers 1:1. When the error type cannot be identified automatically, the annotator is supposed to replace the \tg{X} label on the link between the incorrect form and its correction by one or more error tags. For some error tags, such as \tg{agr} or \tg{dep}, the annotator is instructed to provide a reference link to another word to substantiate the correction. It is usually the agreement source or the syntactic head of the corrected word. 

%
Each annotation was reviewed by a supervisor, who could approve it, modify it, or return it to the annotator with comments for revision.

A subset of the texts annotated this way was independently annotated twice to assess the reliability of the annotation and the robustness of the tagset and the annotation scheme.
After a pilot annotation, we used the result of the comparison to improve the annotation guidelines.
See \refs{sec:error:tiers:quality} for a detailed analysis of errors.
% \jirka{POuzili jsme na to texty pred kontrolou supervisorem?}
% \sasa{Já nevim, řekl bych že spíš ne, že až po kontrole. Ale byl v tom dost zmatek.}



%\bara{Podrobnější popis obr. 7.1 -- co je v jednotlivých částech} \sasa{Lepší?}
\begin{figure}
\centering
\includegraphics[width=.85\textwidth]{img/feat2.png}
\caption{A sentence displayed in the \tool{feat} annotation tool (see 
\autorefpg{tbl:smutnevesely-translated} for the whole text) \exref{NEM\_GD\_008 ru B2}}
\label{fig:process:tiers:feat}
\end{figure}


% referenced from 5.1, refering to 7.6.4, 7.7 and 7.8:


The annotation guidelines do not make any strict requirement about the
sequence of steps in the error annotation, or about the relation of
normalization and categorization as the two error annotation
tasks. They only make an assumption that the two tasks are done by the same
annotator, typically while annotating the whole text in one go.
The annotators tend to normalize and categorize errors at the same
time anyway.
The advantage of this approach is that error tags reflect THs (see
\refs{sec:errors-learn-lang}, p.~\pageref{THvsEC} about the relation
beween TH and error categorization). On the other hand, separating
annotation tasks in time and/or in the person of the annotator can
result in better control of the annotator's judgments and thus more robust
annotation. We followed this idea in \corp{CzeSL-TH} \refS{sec:data:th}, a part of \czesl,
which is corrected at T1 a T2 according to the 2T scheme,
without error categorization. Error tags can be assigned in a separate
step at any time later.

Given the 2T scheme, the annotators can also choose between annotating
whole sentences, paragraphs or texts first on T1 and only then on T2,
or annotating each text in parallel on both tiers. Some annotators
prefer to annotate by paragraphs, first annotating the whole paragraph
on T1 and then on T2, while others annotate by sentences, annotating a
sentence on both tiers in parallel before moving to the next sentence.

Despite the proofread status of the 2T annotation, additional checks
by a different annotator as a part of the MD and implicit annotation
have proved useful. This applies even to the doubly annotated part of
\corp{CzeSL-man}, due to the as yet unrealized plan of its
adjudication. Manual categorization in the MD scheme is based on the
TH made in the 2T scheme (more precisely, on its T2, see
\refs{sec:error:md}). The annotator can modify a TH which is clearly
not correct. However, annotators are discouraged
from substituting a more appropriate TH unless the existing TH is
obviously wrong. In the implicit error annotation scheme
\refS{sec:process:implicit}, the annotators are free to use a TH
suggested in 2T (if the text was annotated in 2T) or to use their own TH.


\subsection{Automatic annotation checking}
\label{sec:process:tiers:auto-check}

The system designed for automatic error tagging is also used for
evaluating the quality of manual annotation, checking the result for
tags that are probably missing or incorrect.
For example, if a T0 form is not known to the morphological analyzer, it is likely to be an incorrect word which should be corrected.
% (except for proper names or words that are intentionally not emended).
Also, if a word was corrected and the change affects pronunciation, but
no error tag was assigned, an \tg{incorBase} or \tg{incorInfl}
error tag is probably missing. This approach cannot find all problems
in error annotation, but provides a good approximate
measure of the quality of annotation and draws the annotator's
attention to potential errors.
\sasa{Znamená to, že to funguje přímo ve featu?}

%%%%%%%%%%%%%%%%%%%
\subsection{Data format for the tiered annotation scheme}
\label{sec:data:xml}
%%%%%%%%%%%%%%%%%%%

% \sasa{původně bylo v kap. o datech, mohlo být taky u czesl-man v1 downloadable?}

To encode the tiered annotation used in \corp{CzeSL-man} \refS{sec:error:tiers}, 
we have developed an annotation schema in the Prague Markup Language
(PML).\footnote{See \citet{pml:2006} and
  \url{https://ufal.mff.cuni.cz/pml}.} \index{Prague Markup Language} \index{XML}
PML is a generic XML-based data format, designed for the representation of rich linguistic annotation organized into tiers. 
Each of the higher tiers contains information about words on that tier,
about the corrected errors and about relations to the tokens on the lower tiers.

We had also considered using a TEI
format.\footnote{\url{https://tei-c.org/}} \index{Text Encoding Initiative}
However, at least for stand-off layered annotation,
\index{format!stand-off} \index{format!multi-tier} the support offered by PML was superior to that of TEI, mainly in
the availability of tools and libraries. 
This concerns tasks such as validation, structural parsing, corpus management and searching. 
While some of those libraries do exist for TEI, many would have to be
developed.

More recently, we started using \tool{TEITOK} \refs{sec:tools:teitok} as the
annotation and search tool, which explicitly supports some parts of
the TEI standard. However, although it allows for stand-off
annotation, its core uses the inline annotation format. \index{format!inline} \sasa{Je
  tohle pořád pravda? ANNIS?}

T0 does not contain any relations, only links to the neighboring T1.
In \autoref{fig:error:tiers:pml}, 
we show a portion (first two words and first two relations) of T1 of
the sample sentence from \autoref{fig:error:tiers:bojal}, encoded in
the PML data format.


%
%The only established alternative supporting layered annotation is the
%tabular format used by \emph{EXMARaLDA}
%\cite{schmidt:2009,Schmidt:etal:2011}. \comsi{je to pořád ještě
%  pravda? není jich víc?} Despite its rich set of options
%and other tools using the format, the format has some drawbacks in a
%scenario involving a language with rich morphology and free word order
%(see, e.g., \cite{hana:etal:10}). Most importantly, the
%correspondences between the original word form and its corrected
%equivalents or annotations at other tiers may be lost, especially for
%errors in discontinuous phrases.
To allow for data exchange, the \tool{feat} editor supports import
from several formats, including \tool{EXMARaLDA}
\citep{Schmidt:2009,Schmidt:etal:2011}; it also allows export limited
to the features supported by the respective format.


%Viz zmeny v sekci Annotation
%\todo{Jirka? Alignment: the reviewer understands that there there are different stages of
%annotations. Probably they are kept apart from each other, standoffish. How do
%you align them so that the result is the interlinear glossed text (IGT)-style
%of presentation seen in the screenshot?}

% \evb{\cite{Hana:etal:2014}}


\begin{figure}%
\begin{footnotesize}
\begin{verbatim}
<?xml version="1.0" encoding="UTF-8"?>
<adata xmlns="http://utkl.cuni.cz/czesl/">
  <head>
    <schema href="adata_schema.xml" />
    <references>
      <reffile id="w" name="wdata" href="r049.w.xml" />
    </references>
  </head>
  <doc id="a-r049-d1" lowerdoc.rf="w#w-r049-d1">
    ...
    <para id="a-r049-d1p2" lowerpara.rf="w#w-r049-d1p2">
       ...
      <s id="a-r049-d1p2s5">
        <w id="a-r049-d1p2w50">
          <token>Bál</token>
        </w>
        <w id="a-r049-d1p2w51">
          <token>jsme</token>
        </w>
         ...
      </s>	
      ...
      <edge id="a-r049-d1p2e54">
        <from>w#w-r049-d1p2w46</from>
        <to>a-r049-d1p2w50</to>
        <error>
           <tag>incorInfl</tag>
        </error>
      </edge>
      <edge id="a-r049-d1p2e55">
        <from>w#w-r049-d1p2w47</from>
        <to>a-r049-d1p2w51</to>
      </edge>
      ...
    </para>
    ...
  </doc>
</adata>
\end{verbatim}
\end{footnotesize}
\caption{A part of T1 of the sample sentence
  (\autorefpg{fig:error:tiers:bojal}) encoded in PML \exref{VOB\_KA\_049 kk A1}}%
\label{fig:error:tiers:pml}%
\end{figure}

\begin{figure*}[htp]
\centering
\begin{footnotesize}
\begin{verbatim}
<Meta>
  <task>
    <id>KAR_MI_005</id>
    <date>2010-04-21</date>
    <medium>manuscript</medium>
    <limit_minutes/>
    <aid>yes|dictionary</aid>
    <exam/>
    <limit_words/>
    <title>Jakou barvu má život?</title>
    ...
  </task>
  <student>
    <id>KAR_MI</id>
    <sex>f</sex>
    <age>19</age>
    <age_cat>16-</age_cat>
    <other_langs/>
    <cz_CEF>B2</cz_CEF>
    ...
    <l1>ru</l1>
    <l1_group>S</l1_group>
  </student>
  <annotation>
    <supervisorId>36</supervisorId>
    <annotatorId>15</annotatorId>
  </annotation>
</Meta>
\end{verbatim}
\end{footnotesize}
\caption{A part of the metadata file KAR\_MI\_005.meta.xml, accompanying the text KAR\_MI\_005 in the PML format}
\label{fig:data:feat:metadata}
\end{figure*}

% \evb{\cite{hana:etal:10}}

