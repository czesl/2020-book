\chapter{Introduction}
\label{sec:intro}



\section{About this book}

%\bara{Tuto část bych dala na začátek + doplnit cíl knížky a target audience}

%\sasa{OK, souhlas. Co je cílem knížky? Představit projekt, ukázat design korpusu, jak sbírat a přepisovat texty, různé koncepce a postupy anotace češtiny jako L2, možná s přesahem do jiných jazyků a do nestandardního jazyka, představit nástroje, ukázat problémy s tím spojené. Kdo je target audience: anyone interested in building and using learner corpora, SLA based on corpus evidence, NLP on non-standard language, teaching Czech as a foreign language, ... ?}

%\sasa{Přesunuto z konce kapitoly na její začátek, doplněn cíl a target audience. Prosím přečíst a schválit.}

The story told in this book began more than ten years ago with the idea to collect Czech, written and spoken by native and non-native learners alike. The aim was to assist experts in teaching Czech as a foreign or native language, but also researchers in the acquisition of Czech. Learner corpora for other languages than English were still quite rare in those days, but the first release of a reference corpus and a treebank of Czech had been available at least since 2000.\footnote{\citet{cznc:00,pdt:00,pdt:2018}} This is why the project was concerned with how to use the methods and tools available for building and using standard corpora of native Czech. Another concern was how to adapt approaches used in learner corpora of other languages, given the specifics of Czech as a language with rich morphology and free word order.
This book reflects these concerns in its focus on annotation, data formats and tools used for building and using the corpus. 

Throughout the years, we have tried and used various solutions. Reports on the achievements and failures are now scattered over a number of papers. We believe it is high time to paint a more orderly picture: remedy inconsistencies, update some claims and figures, and present new, yet unpublished research.

However, cleaning up some mess is not a reason good enough to write a book. Our main aim is to introduce the project, presenting various approaches to the design of a learner corpus, including methods of collecting and transcribing  texts, annotating errors and linguistic categories, and applying computational tools. It could be that some of our experience, positive or negative, may be relevant for other non-native languages than Czech or for other types of non-standard Czech. This is why this book would not be complete without a chapter on the lessons learned.

Who could be interested? The target audience may include anyone interested in fields such as building and using learner corpora, teaching Czech as a foreign language, second language acquisition explored via corpus evidence, or natural language processing of non-standard language. 


\section{Reasons to study non-native Czech}
\label{sec:intro:reasons}

During the last 30 years, the number of non-native speakers of Czech living in the Czech Republic has increased significantly. 
Currently, foreigners constitute at least 5.3\% of the population (more than 16\% in Prague), 
a significant increase from 2.5\% in 2004.\footnote{%
	Figures from 2018; see \citet{Bouskova:etal:2019} or \url{https://www.czso.cz/csu/cizinci/cizinci-pocet-cizincu}. For even more up-to-date and slightly higher figures see \url{https://news.expats.cz/weekly-czech-news/number-of-foreigners-residing-in-the-czech-republic-is-rising/}.
	Note that these figures include only foreigners that registered with the Czech government, \ie they exclude 
	(i) illegal immigrants, 
	(ii) EU nationals that did not register (the registration is optional), 
	(iii) foreigners that reside in the Czech Republic but are registered in another EU country.}
Most of them come from Ukraine, Slovakia and Vietnam, followed by Russia, Poland, Germany, Bulgaria and Romania. 
However, in addition to tourist visitors, many EU citizens who live and work in the country, are not registered. 
Although the percentage of immigrants is much lower than in most other European countries, the sharp upward trend is obvious.

% In 2018, there were 289 thousand foreigners with the status of permanent residence and 275 thousand with the status of long-term residence (over 90 days)
The Czech language has thus become a multi-ethnic communication tool, which is evident particularly in elementary and secondary schools. 
Many foreigners, both in schools and businesses, are learning Czech. 
Czech as a foreign or second language (L2)\footnote{%
	In second language acquisition (SLA), \index{second language acquisition} foreign and second language are different terms. \index{second language}
	A foreign language is acquired in an environment where this language is not generally spoken, a second language is the language learned in a natural environment. 
	Here, we use the two terms as synonymous. See \refs{sec:design:terminology} for more about the concept of L2 as used in this book.} 
is also taught abroad -- \eg in the academic year 2019/2020, the Czech government supported programs at 36 universities and other institutions in 24 countries\footnote{%
	See \url{https://www.dzs.cz/cz/program-podpory-ceskeho-kulturniho-dedictvi-v-zahranici/prehled-lektoratu-a-lektoru/}.} 
and offered courses in most of the 23 Czech Centres abroad, including courses for children.\footnote{See \url{https://www.czechcentres.cz/en/about-us/sit-cc/}.}

This situation brings new challenges.
In comparison with a native language, L2 Czech -- like any other L2 -- is a fairly varied and volatile object. At each stage of learning the language,
Czech of each non-native speaker has its specific vocabulary and grammar structure. \index{second language}
This is why our approach to L2 is based on the theory of interlanguage (IL) \index{interlanguage} as a dynamic system, consisting of developmental stages, through which the learner passes at various stages of proficiency \citep{Selinker:1972}. 
The comparison of non-native and native linguistic production reveals that the utterances of learners form a distinct linguistic system \citep{Tarone:2006}. 
Such a system has been shown to underlie the seemingly random variety of errors made by non-native learners, adults and children alike (see, \eg \cite{Duskova:1969, James:1998}). Patterns of these errors depend on several factors including the speakers’ native language, other languages they might know, the stage and ways of learning the language, etc. Investigating this system is beneficial both to the study of second language acquisition (SLA) \index{second language acquisition} and as a stimulus for the development of teaching methods, instructional materials and software tools for Czech as L2.

Non-native dialects of some other languages, such as English, German or French, are investigated more profoundly.\footnote{See, \eg \citet{dickinson:israel:lee:2010, Boyd:etal:2014, Zinsmeister:etal:2014, Hirschmann:etal:2013}.} Some results of research on non-native languages with richer data resources and a longer research history apply also to non-native Czech (sources of errors, native language influence etc.). However, with its rich inflectional morphology and word order reflecting information structure Czech is typologically different, and many questions about the acquisition of Czech cannot be answered by research concerning a language such as English. On the other hand, research on non-native Czech may be relevant not only to issues of SLA in similar languages (\eg Slavic), but to SLA \index{second language acquisition} in general.\footnote{This can be useful, for example, for determining the acquisition order of phenomena expressing a communicative need such as request, but formed by very different means. Cf.{} the ease of forming the imperative mood in English vs.\ the relative difficulty of producing its Czech counterpart, depending on the morphological paradigm of the verb. }

In recent decades, research of Czech as L2 focused mainly on didactics: Czech was integrated in the Common European Framework of Reference for Languages (CEFR), \index{Common European Framework of Reference} the descriptions of referential levels were established,\footnote{As of 2020, the Czech Ministry of Education has published descriptions for the threshold level and the A1, A2 and B2 levels. See \url{https://www.msmt.cz/mezinarodni-vztahy/referencni-urovne-pro-cestinu-jako-cizi-jazyk} (in Czech). For a general description of the levels see \url{https://rm.coe.int/1680459f97}} a number of new textbooks and teaching materials that reflect the CEFR levels were published, several grammar descriptions intended for non-native speakers were published (\eg \cite{Hercikova:2009}), and new university programs aimed at non-native Czech speakers were introduced.\footnote{Such as Czech Studies for Foreigners at Charles University, Prague: \url{https://ubs.ff.cuni.cz/en/study/courses/bachelor-degree-course/} \jirka{This should be merged with the sentence ending with footnote number 3 - there are other program mentioned.} \sasa{But that sentence is about Czech course abroad. Also, here we talk about recent developments.} and \url{https://ubs.ff.cuni.cz/en/study/courses/the-follow-up-master-degree-programme/}.} Still, issues of the acquisition of Czech and description of its L2 varieties have not received enough systematic attention.\footnote{For studies dealing with the presentation of specific linguistic phenomena, see \refs{sec:using-the-corpus}. \jirka{Ale to je jen vzhledem k nasemu korpusu ne? Tady to vypada jako obecne} \sasa{Obecně je správně, Svatka potvrdila.}}


To summarize, the increasingly stronger position of Czech as L2, the merely intuitive understanding of L2 Czech based on the teacher's experience and the persisting lack of modern didactic support for non-native speakers, including school children, make a strong case for a broadly conceived research, focused on those properties of non-native Czech which are significant, representative, identifiable, amenable to processing by formal tools, and comparable across learner texts of all types.\footnote{This applies especially to  learners with a typologically distant L1, who are not acquainted with the European grammatical categories rooted in Latin.}
% These preferences point to the domain of morphology, which is our main focus due to its key role in Czech as a highly inflectional language and the main source of deviations from the standard.
Therefore, research in non-native Czech is important for both theoretical and practical reasons:

\begin{enumerate}
\item Like every IL, \index{interlanguage} non-native Czech calls for identifying \textit{developmental patterns} \index{developmental patterns} and \textit{orders of acquisition}. \index{orders of acquisition}
	In addition to the research on the sequence of acquisition of L1 structures, there are studies about the acquisition of specific aspects of L2, such as morphemes, pronouns and word order (\eg \cite{Ellis:Barkhuizen:2005}) but not for an inflectional language such as Czech.
	
	\item Analyzing IL is essential for SLA \index{second language acquisition} research, which helps to reveal how language works in general. 
	IL contributes to the understanding of \textit{linguistic universals} in SLA \citep{White:2003}. 
	Investigating SLA of Czech is important because of its typological specifics.
	
	\item Non-native language also offers data for studying \textit{variability} as a key indicator of how a situation affects the learners’ use of L2, either as free variations in the use of a language pattern which has not yet been completely acquired, or as systematic variations, determined by a linguistic, social or psychological context.
\end{enumerate}

Studying these phenomena helps to understand the development of learners’ IL while offering comparison with the acquisition of L1. 
The investigation of IL is worthwhile also in order to find out what types of errors learners make and what the errors say about their knowledge of target language and their ability to use it. 
This is important especially for didactic purposes: 
Czech should be described with regard to non-native speakers, 
and methods for teaching Czech to foreigners need to be elaborated and, i.a., translated into curricula (reflecting the relative difficulty of acquisition of individual phenomena). \index{curriculum}
Analyzing IL is crucial also for language testing \index{testing} (individual features of IL need to be related to standard proficiency levels).

New methodologies, based on extensive data and computational tools, help to advance this line of research. Although unsupervised methods can be used, a formal model must be based on the research of relevant aspects of IL. 
Its absence has also practical consequences. Many NLP tools \index{natural language processing} that are taken for granted (spell checkers with suggestions, Internet search supported by morphology, machine translation, etc.) perform much worse for non-native Czech or are simply unusable, because experts developing applications for the native language cannot rely on previous research. 
Moreover, the study of L2 also has an intrinsic value in itself, as a study of a cultural phenomenon: L2 is part of the non-native speakers’ identity. \index{second language}




\section{Some properties of non-native Czech}
\label{sec:intro:l2Cz}

% \jirka{PRidat odkaz na appendix o cestine - pro porovnani}

Non-native speakers deviate from the standard language in non-arbitrary ways (see, \eg \cite{Ellis:Barkhuizen:2005}); 
the deviations are to a large extent systematic and predictable, but also evolving as learners receive more input and revise their hypotheses about L2 (\cite{Ellis:2003}, 33--35).
They are influenced by:

\begin{enumerate}
    \item The speaker’s native language (\textit{interlingual} errors): \index{interlingual errors}

    \begin{itemize}
    	\item \eet{s matk\eb{oj}}{s matk\textbf{ou}}{with mother} -- Russian ending \e{-oj}  instead of Czech \e{-ou}

    	\item \eet{jsem \eb{v}ietnam\eb{ský}}{jsem \eb{V}ietnam\eb{ec}}{I am Vietnamese} -- adjective (as in English etc.) instead of a noun

        \item \eet{žádný to \eb{ví}}{nikdo to \eb{neví}}{nobody knows it}; lit.: ‘none it not-knows’ -- a single negative form \ee{žádný}{none} (as in English, German, etc.) instead of multiple negative forms \ee{nikdo}{nobody} and \ee{neví}{not-knows}
    
    \end{itemize}


    \item The general properties of the process of acquisition (\textit{intralingual} errors): \index{intralingual errors}

    \begin{itemize}
    	\item \eet{v neděli sp\eb{ám} dlouho}{v neděli sp\eb{ím} dlouho}{I sleep late on Sundays} -- misuse of endings from another inflectional class: \ee{znát}{to know} -- \ee{zn\eb{ám}}{I know} \vs \ee{spát}{to sleep} -- \ee{sp\eb{ím}}{I sleep}, a case of ``false analogy"

    	\item \eet{tady \eb{jsou} pět stol\eb{y}}{tady \eb{je} pět stol\eb{ů}}{there are five tables here}, lit.: ‘there \eb{is} five tables.{\textbf{\footnotesize{GEN}}} here’ -- a case of ``overgeneralization''  from simpler quantifier-free patterns \ee{tady jsou stoly}{there are tables here} \index{overgeneralization}
    \end{itemize}

    \item The properties of the instructional process
\end{enumerate}


Many deviations of non-native Czech as compared with Standard Czech (SCz) \index{Standard Czech} belong to the domains of morphology and morphosyntax.\footnote{For comparison, see an overview of native Czech grammar in Appendix \ref{sec:cze}.} 
This is why we focus on these levels and design a system of concepts capturing the deviations in a systematic, formal and linguistically motivated way. 
The concepts are supported by computational models. \sasa{Opravdu? Asi bych vyhodil} Various contrasts in the patterns of IL can thus be made explicit and linked to parameters such as stages of acquisition \index{orders of acquisition} and differences due to linguistic backgrounds of the speakers. 

%%\bara{Kde focusujeme? V této knížce?} \sasa{Původně zase z grantové přihlášky, v tom grantu jsme se opravdu zaměřili na tohle, ale i z hlediska celého czeslu opravujeme a značkujeme hlavně tohle, taklže by se to asi sneslo bez úpravy, nebo přidat ještě něco o lexiku?}

\subsection{Morphology}

In Czech, as an inflectional language, the syntactic functions of words are mostly expressed by their form, 
whereas word order is to a large extent constrained by information structure. 
Thus the domain of morphology plays a key role in Czech and due to its relative complexity represents the main source of deviations from the standard. It also deserves attention for a practical reason: many tools for computational language processing assume that methodologies and resources concerning morphology are available. \index{natural language processing}

%By analyzing and describing the morphology of non-native Czech we satisfy some of the needs listed above. In the process we are applying and using: (i) theoretical insights about linguistic categories, (ii) methodologies and tools developed within computational linguistics and natural language processing, modified according to the needs of processing learner texts, and (iii) expertise and resources  


%the project, resulting in a learner corpus of Czech, i.e. a collection of texts produced by non-native speakers of Czech, with errors corrected and annotated (\textit{CzeSL}, see, \eg \citet{Sebesta:2012}, or \citet{Hana:etal:2014}).

% \bvb{\cite{Jelinek:2017}}

% \subsection{Czech as a highly inflectional language}

To give an example, Czech nouns have seven cases, with distinct forms for singular
and plural, which means that any noun may have up to 14 different
forms, although in every declension paradigm some forms are identical due to syncretism \index{syncretism} in case and/or number. 
For nouns, there are 14 basic paradigms, and a larger number of paradigm subtypes. 
The paradigm \ee{žen$|$a}{woman} (the most frequent for feminine nouns) has 10 forms, \eg \e{žen$|$ě} is the form for dative and locative singular. 
Also adjectives, pronouns, numerals and verbs have many paradigms and inflected forms.
It is nonetheless not necessary to master the entire Czech
inflectional system in order to successfully communicate in Czech. It
is enough to know how to use the most frequent cases and verbal forms for the common paradigms.
For example, the understanding of the sentence in \refp{ex:intro:uvaly} is not disrupted by the error \et{Úvalách}{Úvalech}.\footnote{A parenthesized  morphosyntactic category in the gloss, such as \cat{(loc)}, denotes an intended use of the category in an incorrect form. For a list of all conventions used in the examples, including identification of the source text, see Appendix \ref{sec:examples}.}

% \begin{exe}
% \ex\label{ex:intro:uvaly}
% \gll Oslavil jsem Vánoce se svými příbuznými v jejich domě v *Úvalách.\\
%     Oslavil jsem Vánoce se svými příbuznými v jejich domě v Úvalech.\\
% \glt ‘I celebrated Christmas with my relatives at their home in Úvaly.’\\
% \end{exe}

\begin{exe}
\ex\label{ex:intro:uvaly}
\gll Oslavil jsem Vánoce se svými příbuznými v jejich domě v \e{*}Úval\textbf{á}ch {\arrow} Úval\textbf{e}ch.\\
celebrated \textsc{aux} Christmas with self's relatives in their
     house in Úvaly\cat{(loc)} { } Úvaly\cat{loc}\\
\glt ‘I celebrated Christmas with my relatives at their home in Úvaly.’ \\ \exref{KAR\_MD\_020 ru A2+}
\end{exe}



\jirka{dale pouzivame konvenci s hvezdickou u padu, pouzival bych to vsude. } \sasa{Jenže dál je hvězdička u morfologických kategorií jen tam, kde je blbě kategorie, ne její vyjádření. Sem patří hvězdička k formě, ne ke kategorii. To se v anglický glose těžko vyjádří. Dávám hvězdičku v glose pryč. Místo toho dávám závorku,jako že "intended". Přidal jsem footnote, je to tak srozumitelný?}

The error \et{Úvalách}{Úvalech} \index{error!inflection}
is in the form of the locative plural of
the name of a Czech town \e{Úvaly}, a plurale tantum: 
the case ending \mbox{\e{-ách}} used incorrectly instead of \mbox{\e{-ech}} is an existing ending, 
used to express the same morphosyntactic properties 
of nouns of another paradigm (\eec{Roztokách}{Roztoky}{loc}). 
As the incorrect form denotes the same nominal case of the noun, 
it may be noticed as unexpected by a native speaker, but it will not hinder the
understanding of the whole sentence.
Another type of inflection error, found in \refp{ex:intro:rodina}, 
can make the sentence slightly less understandable.

% \begin{exe}
% \ex\label{ex:intro:rodina}
% \glll V životě dávám přednost *rodinu.\\
% V životě dávám přednost rodině.\\
% In life give precedence family.\\
% \glt‘In my life, I prefer family.’\\
% \end{exe}

\begin{exe}
\ex\label{ex:intro:rodina}
\gll V životě dávám přednost \e{*}rodin\textbf{u} {\arrow} rodin\textbf{ě}.\\
in life give\cat{1sg} precedence family\cat{*acc} { } family\cat{dat}\\
\glt ‘In my life, I prefer family.’ \exref{HRD\_AS\_221 ja A2}\\
\end{exe}

The form \ee{rodinu}{family} is a form for accusative singular of the noun \e{rodina}, 
in a construction where the dative form \e{rodině} is expected. \index{error!case}
The sentence is still understandable, as it is composed of
only a few words, but the use of incorrect case makes it more challenging
to be understood by a native speaker.
When a completely random ending is used, unrelated to paradigm or case
form, the understanding is even more disrupted.

% \evb{\cite{Jelinek:2017}}


% \jirka{Nasledujici dva odstavce bych vynechal. V priklasce ke grantu to davalo smysl, tady ne} \sasa{Souhlas!}

% \textit{Morphological analysis} (MA) assigns tags encoding the word’s morphological properties out of context; \textit{morphological tagging} assigns each word a single tag based on the context. Czech tagsets use several thousands tags (4000+ in the Czech Positional tagset, see \citet{Hajic:2004}). MA and tagging may be accompanied by lemmatization, a procedure assigning each word its lemma (also called base/canonical form). MA, tagging and lemmatization are essential for many Natural Language Processing (NLP) applications, such as syntactic parsers, grammar checkers, speech recognition systems, etc.

% Unfortunately, there are no tagsets, taggers or analysers suitable to non-native Czech. As our previous research  shows, the models of Standard Czech perform poorly on non-native Czech, mainly due to the large number of non-words, idiosyncratic syntactic patterns and different statistical distributions of words and constructions \citep{Stindlova:2011b}. This implies a poorer performance of all other methods and tools which require information about morphology.

\subsection{Syntax}

Most syntactic deviations are found in morphosyntax, 
often when forms are lexically determined, \eg by valency as in \refp{ex:intro:rodicich} \index{error!valency}
or subject to a principle of grammar, \eg agreement, as in \refp{ex:intro:studentu}. \index{error!agreement}


\begin{exe}
\ex\label{ex:intro:rodicich}
\strexample
{myslím \e{*}\eb{o} tobě}
{think\cat{1sg} about you}
{myslím \eb{na} tebe}
{think\cat{1sg} on you}
{I'm thinking about you}
{DGD\_L5\_143 ru A1}
\end{exe}

% Těším se na tvůj dopis, myslím o tobě 	DGD_L5_143 ru A1


\begin{exe}
\ex\label{ex:intro:studentu}
\stexample
{\e{*}přišl\eb{i} pět studentů}
{came\cat{*pl.*ma} five students\cat{gen}}
{přišl\eb{o} pět studentů}
{came\cat{sg.neut} five students\cat{gen}}
{five students came}
\end{exe}

% V rodině byli jsme pět - táta, mátka, dve sestři a já. HRD_1S_197 en A2




Other deviations include non-standard word order due to an inappropriate topic-focus articulation (information structuring), \index{error!word order} or due to a misplaced clitic, such as in \refp{ex:intro:studovani}, where \ee{jsem}{am} and \e{se} – reflexive particle – are both 2nd position clitics \index{clitic} and should follow the first constituent \ee{během studování na univerzitě}{during university studies} in that order. 

\begin{exe}
\ex\label{ex:intro:studovani}
\strexample
{během studování na univerzitě \e{*}\eb{se} seznámil \e{*}\eb{jsem} s Evou}
{during studying on university \catt{refl} met \catt{aux}\cat{1sg} with Eva}
{během studování na univerzitě \eb{jsem} \eb{se} seznámil s Evou}
{during studying on university \catt{aux}\cat{1sg} \catt{refl} met with Eva}
{during my university studies I met Eva}
{BLAH\_DZ\_001 ky B2}
%\exref{BLAH_DZ_001 ky B2}
\end{exe}

Similarly, reflexive pronouns are often under-used, as in \refp{ex:intro:praci}, where the possessive \ee{moji}{my} should be replaced by the reflexive possessive \e{svoji}. \index{error!reflexive}

\begin{exe}
\ex\label{ex:intro:praci}
\strexample
{miluji \e{*}\eb{moji} práci}
{love\cat{1sg} my work}
{miluji \eb{svoji} práci}
{love\cat{1sg} self's work}
{I love my work}
{TOD\_P2\_247 ru A2+}
\end{exe}

% Miluji moji práci, protože je zajímavá a zodpovědná. TOD_P2_247 ru A2+

Various types of errors can occur within the same sentence, as in \refp{ex:intro:sestri}. 

\begin{exe}
\ex\label{ex:intro:sestri}
\stexample
{V rodině \e{*}byl\eb{i} \e{*}\eb{jsme} pět -- táta, \e{*}m\eb{á}tka, \e{*}dv\eb{e} \e{*}sest\eb{ři} a já.}
{in family be\cat{*pl.*ma} \catt{aux}\cat{1pl} five { } dad (mother) (two) (sisters) and I}
{V rodině \eb{nás} byl\eb{o} pět -- táta, m\eb{a}tka, dv\eb{ě} sest\eb{ry} a já.}
{in family we\cat{gen} be\cat{3sg.neut} five { } dad mother two sisters and I}
{We were five in my family -- father, mother, two sisters and me.}
\exref{HRD\_1S\_197 en A2}
\end{exe}

In \refp{ex:intro:sestri}, the three errors in morphology and morphonology (\e{mátka}, \e{dve} and \e{sestři}) \index{error!inflection} are combined with an error in the agreement pattern involving quantified subject, shown in \refp{ex:intro:studentu}. \index{error!agreement} 
The quantified subject NP, agreeing with a verb form in the 3rd person neuter singular (like \eec{přišlo}{came}{3sg.neut} in \refp{ex:intro:studentu}), includes the genitive form of the first person plural pronoun (\e{nás} -- in the source sentence assumed to be nominative and thus pro-dropped). \index{error!pronoun} On the other hand, the 1st person plural past tense auxiliary \e{jsme} is dropped in the target sentence, because there is no auxiliary in the 3rd person past tense. 

% V rodině byli jsme pět - táta, mátka, dve sestři a já. HRD_1S_197 en A2

\subsection{Word segmentation}

Inappropriate word segmentation is not a random phenomenon either. \index{error!word boundary} 
Words are often incorrectly split after prefixes homonymous with prepositions (\ee{do psat}{in write} instead of \ee{dopsat}{finish writing}).
\index{homonymy}
Russian speakers influenced by their native language sometimes 
append reflexive pronoun to the verb (\eet{smějuse}{směju se}{I laugh}) or 
split verb and the negative particle (\eet{ne studuju}{nestuduju}{I don’t study}).
\index{error!reflexive}
%\jirkas{However, ignoring word boundaries is a problem mainly for learners primarily exposed to spoken Czech,  where words such as clitics and prepositions form prosodic units with their hosts,}{
Since clitics, \index{clitic} such as some prepositions and short pronouns, form prosodic units with their host, speakers exposed primarily to spoken Czech might spell them incorrectly as one word, as in \refp{ex:intro:opalimse}.\footnote{Example \refp{ex:intro:opalimse} is from \corp{SKRIPT 2015}, a corpus of young native Czech learners. The text ID is followed by the code
  for Czech (cs) and the age of the author.}

%n\jirka{Since clitics are not full words, we cannot say that speakers ignore word boundaries.}

%\ee{\eb{opálímse} ale \eb{musímít} opalovací krém abych \eb{semse} nespálil moc}{I will get a sun tan but I must have a sunscreen so that I would not get sunburnt too much}, lit.: ‘[I] \eb{suntan+myself} but \textbf{must+have} sunscreen so that [I] \textbf{wouldn’t+myself} burn too much’.


% \begin{exe}
% \ex\label{ex:intro:opalimse}
% \gll *\eb{opálímse} ale *\eb{musímít} opalovací krém abych *\eb{semse} nespálil moc \\
%       suntan\cat{1sg}+\textsc{refl} but must\cat{1sg}+have sunscreen so that wouldn’t+myself burn too much \\
% \glt `I will get a sun tan but I must have a sunscreen so that I would not get sunburnt too much.'\\
% \end{exe}

\begin{exe}
\ex\label{ex:intro:opalimse}
\strexample
{\e{*}\eb{opálímse} ale \e{*}\eb{musímít} {opalovací krém} abych \e{*}\eb{semse} nespálil moc}
{suntan\cat{1sg}+\textsc{refl} but must\cat{1sg}+have sunscreen {so-that-\textsc{aux}}\cat{1sg} \textsc{aux}\cat{1sg}+\textsc{refl} burned\cat{neg} {too much}}
{\eb{opálím} \eb{se} ale \eb{musím} \eb{mít} {opalovací krém} abych \eb{se} nespálil moc}
{suntan\cat{1sg} \textsc{refl} but must\cat{1sg} have sunscreen {so-that-\textsc{aux}\cat{1sg}} \textsc{refl} burned\cat{neg} {too much}}
{I will get a sun tan but I must have a sunscreen so that I would not get sunburnt too much.}
{ss\_dp\_057\_63 cs 11}
\end{exe}


\section{Learner corpus}
\label{sec:intro:learner}

Investigating language acquisition by non-native learners helps to understand important linguistic issues and to develop teaching methods, better suited both to the specific target language and to specific groups of learners. 
These tasks can now be based on empirical evidence from learner corpora.

A learner corpus consists of language produced by language learners, typically learners of a second or foreign language (L2). 
Such corpora may be equipped with morphological and syntactic annotation, 
together with the detection, correction and categorization of non-standard linguistic phenomena.

Learner corpora allow to compare non-native and native speakers’ language, or to compare interlanguage \index{interlanguage} varieties, and can be studied on the background of standard reference corpora, which helps to track various deviations from standard usage in the language of non-native speakers, such as frequency patterns – cases of overuse or underuse – or \textit{foreign soundingness} as compared with the language of native speakers. A range of studies have focused not only  on the frequency of use of individual elements of language (\eg \cite{Ringbom:1998}), including phenomena such as negative and positive transfer, formulaic language, collocations (lexical patterns), prefabs and colligations (lexico-grammatical patterns, \eg \cite{Nesselhauf:2005,Paquot:Granger:2012,Ellis:2017,Granger:2017,Vetchinnikova:2019}),  lexical analysis and phrasal use (\eg \cite{Altenberg:1998}), but also developmental patterns, variability and the impact of the learning context \citep{Meunier:2019, Granger:etal:2015} and interlanguage complexity (\eg \cite{Paquot:2019}).

An error-tagged corpus can be subjected to \textit{computer-aided error analysis} (CEA), \index{computer-aided error analysis} which is not restricted to errors seen as a deficiency, but understood as a means to explore the target language and to test hypotheses about the functioning of L2 grammar. 
CEA also helps to observe meaningful use of non-standard structures of IL. 
Such studies focus on lexical errors (\eg \cite{Lenko-Szymanska:2004}), wrong use of verbal tenses (\eg \cite{Granger:1999}) or phrasal verbs (\eg \cite{Waibel:2008}).

The tasks of designing, compiling, annotating and presenting such
corpora are often very much unlike those routinely applied to standard
corpora. 
There may be no standard or obvious solutions: the approach to
the tasks is often seen as an answer to a specific research goal
rather than as a service to a wider community of researchers and
practitioners. 

%Our aim is to investigate some of the challenges, based on a learner corpus of Czech in comparison to several other learner corpora\bara{Jak se má tento cíl k obsahu knížky?}.

The difference between a standard and a learner corpus is mainly in their annotation. Texts in a learner corpus can be annotated in two independent ways: 
(i) by standard linguistic categories: morphosyntactic tags, base forms, syntactic structure and functions, and 
(ii) by error annotation: correct version of each ill-formed part of the source text, \ie its target hypothesis (TH), \index{target hypothesis} and categories specifying the nature of errors. 
Reasonably reliable methodologies and tools are available for linguistic annotation (i) of many languages, as long as the text is produced by native speakers. 
The situation is different for non-standard language of non-native learners and for error annotation (ii), 
where manual annotation is quite common. 
However, with the growing volumes of learner corpora, the need for methods and tools simplifying such tasks is increasing. Yet the annotation of learner corpora remains a challenging task, 
even more so for a language such as Czech, with its rich inflection, derivation, agreement, and a largely information-structure-driven constituent order. 
% A typical learner of Czech makes errors across all linguistic levels, often targeting the same form several times.


\section{Roadmap}
\label{sec:intro:roadmap}

%After the Introduction, providing some motivation for studying non-native Czech, listing some of its properties and introducing learner corpora as a useful resource, the rest of the book reads as follows.

\begin{description}
	% Introduction
	% \item[\refs{sec:intro}:] \nameref{sec:intro}
	
	% Learner Corpora
	\item[\autoref{sec:corpora}:] \textbf{\nameref{sec:corpora}} provides some context by listing several properties which make each learner corpus different from any other. The second half of the chapter presents an overview of nine learner corpora with features relevant for the \czesl corpus.
	
	% Design and overview
	\item[\autoref{sec:design}:] \textbf{\nameref{sec:design}}
	presents the foundations of the \czesl project and outlines its main characteristics.
	
	% Transcription, anonymization, metadata
	\item[\autoref{sec:tam}:] \textbf{\nameref{sec:tam}} 
	deals with the initial tasks in the compilation of the \czesl corpora. It is the first in the sequence of three chapters concerned with how the texts are treated and what kind of annotation they receive. These chapters do not focus on the actual pre-processing, which is the topic of Chapter \ref{sec:data}, but rather on the description of the principles, categories and formats. 
	
	% Error annotation
	\item[\autoref{sec:error}:] \textbf{\nameref{sec:error}} 
	presents the background and substance of several types of error annotation used in the \czesl project. We focus on the original error annotation scheme, consisting of three parallel tiers for the source text and two tiers for its annotation. This type of error annotation is examined from several angles: we provide motivation behind this design, present the grammar-based and the ``formal'' error tagsets, complementing each other, and provide results of its evaluation in terms of inter-annotator agreement (IAA). The chapter follows by introducing two additional types of error annotation used in the \czesl project more recently: annotation without explicit error tags, facilitating manual annotation, and a multidimensional scheme, complementing the original tiered system especially in the domain of morphonology.
	
	% Linguistic annotation
	\item[\autoref{sec:ling}:] \textbf{\nameref{sec:ling}} 
	examines the approaches adopted in \czesl to the annotation of morphosyntactic categories, syntactic structure and functions. The chapter consists of three main parts: it starts with the methods analyzing the TH, proceeds to methods developed for standard language but used to annotate source learner texts, and concludes with the description of an approach to syntactic analysis designed specifically for learner Czech.
	
	% Annotation process
	\item[\autoref{sec:process}:] \textbf{\nameref{sec:process}}
	 looks at the transcription, anonymization and annotation from the perspective of a step-by-step procedure, including decisions about the share of manual tasks and suitability of automatic tools. The various types of annotation described in the preceding chapters are here described in terms of input, processing and output.
	
	\sasa{Kapitola se jmenuje o anotaci, ale je tam i transkripce a anonymizace, změnit název? Ale nevím jak.}
	
	% Data
	\item[\autoref{sec:data}:] \textbf{\nameref{sec:data}}
	provides an overview of searchable corpora or downloadable data sets containing the \czesl texts. The various releases reflect the various approaches to the annotation, but they also differ in the choice of texts, availability of metadata and the data format, determining the search options, \ie the choice of a suitable search tool.
	
	% Tools
	\item[\autoref{sec:tools}:] \textbf{\nameref{sec:tools}}
          is an overview of tools used within the \czesl project for processing and annotating texts on the one hand and for searching and viewing them on the other.

          % Some of the tools were developed within the project.
	
	% Using the corpus
	\item[\autoref{sec:using-the-corpus}:] \textbf{\nameref{sec:using-the-corpus}}
	is concerned with how the \czesl corpora are used in research and teaching of Czech as a foreign language, and also in NLP applications such as text scoring, text correction and natural language identification (NLI). Some of these types of use are closely related with the exploitation of standard reference corpora for the same purpose, which is why a section about the use of corpora of native Czech is also  included. 
	
	% Lessons learnt and perspectives
	\item[\autoref{sec:lessons}:] \textbf{\nameref{sec:lessons}}
	concludes the core chapters of the book by discussing  positive and negative experience from implementing various solutions throughout the project and by an outlook into the future.
	
	% Acknowledgements
	\item[\autoref{sec:acknowledgements}:] \textbf{\nameref{sec:acknowledgements}}
	should be seen as an important part of the book. There are many people and several funding agencies who deserve our credit for starting the project and for keeping the project alive throughout the years.
	
	% Appendix: Notes about Examples
	\item[\autoref{sec:examples}:] \textbf{\nameref{sec:examples}}
	briefly summarizes the presentation of examples.

	% Appendix: About the Czech language
	\item[\autoref{sec:cze}:] \textbf{\nameref{sec:cze}}
	presents an overview of Czech as a native language in its main features. This part may be useful especially for readers who do not speak or understand Czech.
	
\end{description}



