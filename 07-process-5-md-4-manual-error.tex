\subsection{Manual error annotation}
\label{sec:process:md:manual}

\index{annotation!manual}
\index{error!domain}
The manual MD annotation is done in the \tool{brat} annotation editor \refS{sec:tools:brat}. The design and principles of the MD annotation scheme are described in \refs{sec:error:md} above.\footnote{For the MD annotation manual (in Czech) see \citet{Skodova:etal:2019}.} 

\autoref{fig:error:md:brat-leszek} shows a text in \tool{brat}, while \autoref{fig:error:md:brat-leszek-tags} shows the same text with a menu of error tags -- labels of domains and features. The text has been pre-processed and manually annotated. As described above, pre-processing involves a partial morphemic analysis and automatic error annotation.%
\footnote{A pilot annotation of 18 texts, based on the annotation manual,  can be viewed and searched using \tool{brat} at \url{https://quest.ms.mff.cuni.cz/brat/czesl.err/index.xhtml\#/anna_daniela/}, or downloaded as a dataset in the \tool{brat} format from \url{https://bitbucket.org/czesl/czesl-md/}.}

\begin{figure}[htb!]
\includegraphics[width=\textwidth]{img/brat-leszek.png}

  \caption{A sample MD annotation in \tool{brat} \exref{AA\_AO\_002 pl B1}}
  \label{fig:error:md:brat-leszek}
\end{figure}

\begin{figure}[htb!]
\includegraphics[width=\textwidth]{img/brat-leszek-tags.png}

  \caption{MD annotation in \tool{brat} with the error tags menu \exref{AA\_AO\_002 pl B1}}
  \label{fig:error:md:brat-leszek-tags}
\end{figure}

Each sentence in \autoref{fig:error:md:brat-leszek} is displayed twice. The pre-processed source version, corresponding to T0, comes first. Inflective words are split into morphs by asterisks and errors are tagged by error labels, corresponding to the feature name. Nearly all errors are detected and partially categorized automatically in the pre-processing step. The annotators proofread and modify the annotation by comparison with the target hypothesis of the sentence, shown below the source on the light grey background. The target hypothesis is adopted from the 2T annotation scheme. It is assumed to be correct, but can be modified when the annotator disagrees. 

For space reasons, the error domains are distinguished by different colors rather than by more verbose display of the domain tag with a gloss. Error labels assigned in pre-processing are denoted by the letter ``a'' preceding the feature tag, as in \tg{aDIA}. Some of the other \tg{a}-type tags may have been modified by the annotator, other tags were added manually. The span of the error, shown below the error tags, may be identified correctly by the morphemic analysis, but the annotator is free to modify it or annotate a new error with its specific error span. 

One of the main features of the MD annotation scheme is the option of multiple alternative interpretations of an error even in the case of a single target hypothesis.
\index{alternatives!categorization}
This might seem as an additional burden for the annotator. However, there are some regular patterns of co-occurring error tags, which are used in the pre-processing and post-processing steps. The patterns follow from the error taxonomy and most of them are easy to remember (\cf \autoref{tbl:error:md:feetimpl} and \autorefpg{tbl:error:md:feetcooc}).

For example, a feature tag in the \tg{MPHON} domain is deducible from an automatically assigned tag in the \tg{ORT} domain. As a result, the annotator need not worry about the annotation in the \tg{MPHON} domain when an \tg{ORT} domain tag is in place. The same rule applies also in the opposite direction. On the other hand, tags in the \tg{LEX} domain, except for \tg{CHOICE}, and in the \tg{SYN} domain must always be specified by hand. However, if the \tg{SYN} domain tag is \tg{AGR} or \tg{DEP}, then the \tg{FLEX} tag in the \tg{MORPH} domain is always appropriate and need not be specified, and -- if either \tg{ALT} or \tg{CHAR} are the correct tags in the \tg{MPHON} domain -- they are not needed either.

To assist the annotator, the annotation editor is  informed about possible combinations of tags and issues a warning whenever the annotator adds an incompatible tag for the same or overlapping span.

\index{error!span}
The annotation spans can be of arbitrary length, from a single character to a sequence of words, where some words need not be completely included in the span. For some error types (defined in the \tool{brat} annotation setup), even discontinuous sequences of words or characters are allowed. These error types include \tg{ORT:SEG}, \tg{MORPH:VBX}, \tg{SYN:WO} or \tg{LEX:PHR}. For multiple errors concerning different parts of a single word form, it is useful to specify spans for multiple substrings of a word, \ie a morph or even a single character. On the other hand, some tags can only be used for entire words. This applies mainly to the \tg{LEX} and \tg{SYN} domains, except for \tg{SYN:AGR}, \tg{SYN:DEP}, \tg{LEX:NEG} and \tg{LEX:USE}. 

The MD annotation can be searched and viewed also in \tool{TEITOK} \refS{sec:tools:teitok}.  \autoref{fig:error:md:teitok-brat} shows the same text again, this time in the \tool{TEITOK} stand-off annotation view. The view shows only the MD annotation. Annotated words are underlined, details of the annotation are shown on mouse-over. In the right-hand column the error codes used in the text are listed at the top. A click on the tag shows all words annotated by that tag. All annotated forms with the spans highlighted are shown in the list of similarly clickable forms below the error tags. 

\begin{figure}[htb!]
\includegraphics[width=\textwidth]{img/teitok-brat.png}

  \caption{A sample of MD annotation in \tool{TEITOK} \exref{AA\_AO\_002 pl B1}}
  \label{fig:error:md:teitok-brat}
\end{figure}

In \tool{TEITOK}, the MD annotation can also be edited. Error tags can be deleted or added, and the span and error tag can be modified. 





