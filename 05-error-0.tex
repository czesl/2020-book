\chapter{Error annotation}
\label{sec:error} 


%%\bara{Pro každé anotační schéma jedna kapitola? A ne sekce.}
%%\sasa{Nevím:  2T má 35 stránek, MD 9 a Implicit 3}

\section{Errors and learner language}
\label{sec:errors-learn-lang}

Corpus-based research of learner language inherited its two main
methodologies from the field of second language acquisition (SLA):
contrastive analysis (CA) and Error Analysis (EA).
\index{second language acquisition}
\index{contrastive analysis}
\index{error analysis}
The main focus of
CA is the comparison of the language of the learner with the native
language, typically resulting in data about the underuse or overuse of
specific linguistic phenomena. For CA,
learner errors in the corpus need not be annotated, it is enough to
identify comparable exponents of the researched features in the native
and the learner language. On the other hand, it is hard to imagine a
corpus-based EA study without an explicit identification of such
errors in the corpus. This is the reason why some error annotation is
available for most learner corpora, and also why learner corpora are
typical by including error annotation as their specific
feature.\footnote{See, \eg \citet{Diaz-Negrillo:2006}.}

To design and implement error annotation is not an easy
task. \index{annotation!error} Despite
a rich pool of literature devoted to the subject, only few solutions
seem to be reused in new projects. Yet a thorough research of
available options is advisable. Discussions of learner language
predating the boom of learner corpora often concern issues recurring
in contemporary efforts to design and build resources optimally suited
to the language and the goals of the project.\footnote{For overview and
  references see, \eg \citet{Ludeling:Hirschmann:2015,Stemle:etal:2019}.}

There are many points where the \czesl project touches upon such
issues. Staying with the topic of this chapter, one of the key points
is the status of error. At least since \citet{Corder:1967}, errors are
treated as a necessary part of language acquisition and an important
indication of the discrepancy between the learner's competence in L2
and its native counterpart. Once the hypothesis of learner's internal
grammar (IL) \index{interlanguage} is accepted \citep{Selinker:1972}, errors can be treated
 as an important source of knowledge about the hidden system. However,
only errors reflecting the system (competence errors) rather than
random errors (performance errors or mistakes) are relevant for the
study of IL \index{interlanguage} \citep[166]{Corder:1967}. The problem is that the two
error types can be more or less reliably distinguished only by
analysing all data available from a specific source. It is impossible
to decide separately for each individual instance, thus errors and
mistakes cannot be told apart during annotation. To remedy that,
\czesl, like most other learner corpora, offers metadata about the
learner, usable to decide about errors occuring in texts of a specific
learner or a group of learners.

A more profound problem related to errors in relation to IL \index{interlanguage} was
pointed out by \citet{Bley-Vroman:1983}:{} errors are the result of
comparison of a language as L2 with the same language as L1. Errors
are therefore a concept seen from the perspective of a native
speaker. On the other hand, the learner's internal grammar (IL) and
its evolution can be based on categories and principles very different
from those of the target language. \citet[4]{Bley-Vroman:1983} sees
the reliance on errors in the effort to describe IL as ``a departure
from the original spirit of the interlanguage hypothesis as advocated
in \citet{Selinker:1972}'', as something actually preventing to see
the systemacity of the learner language:

\begin{quote}
  Language systems are to be considered in their own right, on the
  basis of their own “internal logic.” The structuralist linguistic
  rejection of what were perceived to be “Latin-based” grammars must
  be seen in this light. Languages which seem absurd and illogical
  when viewed from the standpoint of Latin turn out to be reasonable
  and (as we would say) systematic when allowed to stand on their
  own. In the same way that we would not judge the systematicity of
  Nootka by comparing it with Latin (or even with Kwakiutl) we do not
  appropriately measure the internal systematicity of an interlanguage
  by comparing it with another (albeit related) language, the target
  language. \citep[15]{Bley-Vroman:1983}
\end{quote}



Yet we agree that ``many of the properties of learner language can
only be understood if learner language is compared to target language
structures'' \citep[155]{Ludeling:Hirschmann:2015} and see error
annotation as crucial for studying learner language. Rather than
obscuring the access to IL, appropriately used error annotation can be
used as a platform for studying IL \index{interlanguage} from various angles.


% --- the nature of errors ---

% - errors reflect interlanguage, a system consisting of the learner's
% internal grammar,

% but only ``competence'' errors
% count, while ``performance errors'' or ``mistakes'' are due to
% processing issues and not
% relevant for the study of interlanguage

% but errors and mistakes cannot be distinguished by looking at
% individual instances, but only in an analysis of all available data,
% thus they cannot be annotated

% \cite{Bley-Vroman:1983} objects to the systematicity of interlanguage
% and questions the validity of EA which falls into the trap of
% comparative fallacy, because it is always done from the
% perspective of a native speaker.
% czesl?

There is also a more practical issue, namely the issue of what counts
as an error. \index{error} While some errors are definable as violating a grammar
rule (in terms of \cite[701]{Ellis:1994}, ``overt errors''), native
speakers often do not agree about cases of inappropriate but still
grammatically correct use (``covert errors''). The latter tend to
occur especially in the language of advanced learners and are
perceived as features typical for a non-native speaker rather than as
errors as such.  Acknowledging the grey zone,
\citet[182]{Lennon:1991} defines an error vaguely in contrast to
what a native speaker would produce in the same context and under
similar conditions.

% \begin{quote}
%   a linguistic form or combination of forms which, in the same
%   context and under similar conditions of production, would, in all
%   likelihood, not be produced by the speakers' native speaker
%   counterparts
% \end{quote}

We tried to be less vague in \czesl by using SCz as the yardstick
wherever the codified norm offered any support, in the grammar or in
the lexicon, or by discouraging annotators from correcting/annotating
stylistically inappropriate expressions unless they impede
understanding or sound very unnatural \refS{sec:error:general}. The
strategy to keep the annotators' intervention with the text at a
minimum is probably motivated by similar concerns and can have the
same effect as the ``principle of positive assumption,'' intended to
prompt the \index{principle of positive assumption}
preference for interpreting the text to the learner's advantage
\citep{Volodina:etal:2016,Volodina:etal:2019}.\footnote{Strictly
  speaking, the principle of positive assumption formulated in
  \citet{Volodina:etal:2016,Volodina:etal:2019} concerns the strategy
  of resolving uncertainty about the interpretation of written text,
  but it seems to be also applicable to the decision whether a
  borderline case should be counted as an error.}



% --- the definition of an error ---

% - violation of a rule, but language is not governed by rules only:
% grammar errors \vs appropriateness (usage) errors, or, in terms of
% \cite[p.~701]{Ellis:1994}, overt \vs covert errors

% - \cite[p.~182]{Lennon:1991}: a form which a native speaker would not
% be likely to use in the same context and under similar conditions (idioms)


Some errors can be detected and corrected within a very restricted
span of text, \eg a word, a morph or even a single character. \index{error!span} Other
errors may require a much larger context (a constituent, clause,
sentence, paragraph or even the entire text) or even a glimpse into
the extralinguistic situation. \citet[191]{Lennon:1991}
conceptualizes the range of context needed for the error to become
apparent as ``error domain'', and the span of text which needs to be
repaired as ``error extent''. There is some correlation between the
two notions on the one hand and the grammar-based type of error. \Eg
in English, an error in the choice of a preposition could have a
domain spanning a clause, while its extent would be limited to the
preposition. In case-marking languages the extent could include the NP
following the preposition.

In \czesl, annotators are instructed to find a TH with regard to the
context and the probable intention of the author, while at the same
time making sure that it is as close as possible to the original, \ie
to minimize the changes of word forms, word order, lexical setting, number of
words, etc. In this sense, the principle of contextual interpretation
is applied in parallel with the principle of minimal intervention.

However, error domain and error extent are not primitive concepts in
any of the \czesl error annotation schemes. They are only implied in
the tiered (2T) error annotation \refS{sec:error:tiers}. For cases
when the domain and the range coincide, the annotation scheme uses
links connecting the source word forms with their TH counterparts. The
domain of an error is specified by linking potentially multiple (even
non-contiguous) source forms and expanding the joined links again as
potentially multiple links targeting the correct forms, representing
the error extent. This mechanism is used for cases such as joining and
splitting forms, changing word order or correcting multi-word
units. For cases when the domain is wider than the extent, the scheme
uses pointers to forms motivating the correction, \eg to a subject as
the agreement source for a predicate form supposed but failing to
agree. For cases when a wider context must be repaired, the notion of
a follow-up or secondary error is used
\refS{sec:error:tiers:compromise:follow-up}, as in the above example
involving the replacement of a preposition. If the correct preposition
assigns a different case to its NP, the corrected NP is tagged as
subjected to a follow-up correction.\footnote{Unlike in \czesl, where
  the implied domain and extent are specified in terms of word forms
  (tokens), \citet{Lennon:1991} delimits their range by linguistic
  units: morphemes, words, constituents, sentences. This can be the
  reason why in \czesl the extent of some errors appears to be wider
  than their domain, as in some follow-up corrections, contrary to
  \citet[192]{Lennon:1991}: ``for any given error, domain will be
  at a higher rank than or equal rank to extent, but never at a lower
  rank''.}

A single form can be diagnosed as erroneous for several
reasons. \citet[193]{Lennon:1991} points out the special case of
embedded errors, as in \et{he seems to be drunken}{he seems to be
  drowned}. \index{error!successive} The morphologically incorrect form \et{drunken}{drunk}
must still be corrected as \et{drunken}{drowned}. Lennon suggests to
ignore the embedded (intermediate) error for the purposes of error
counting (and, we might add, error annotation). His main reason is not
the impossibility of a different solution, but concern about
consistency of the error analysis. In \czesl, we tried both ways:
embedded errors are annotated in the 2T \refS{sec:error:tiers} and
implicit schemes \refS{sec:error:implicit} and ignored in the MD
\refS{sec:error:md} and UD schemes \refS{sec:ling:ud}. The former
extracts more information, the latter simplifies the process.

% error domain in 2T: 
% a token or smaller at T1, larger than a token at T2
% extent in 2T: 
% a token, unless multiple incoming edges (joining, word order, constructions;
% follow up corrections!

Discussion of error analysis and error annotation cannot avoid the
role of TH. Is an explicit formulation of TH needed for annotating
(error-tagging) each error? Are multiple successive or even
alternative THs needed? Can errors be identified only by
THs? \label{THvsEC}
\index{target hypothesis}

According to \citet[141]{Ludeling:Hirschmann:2015}, ``Errors cannot
be found and analysed without an implicit or explicit target
hypothesis – it is impossible not to interpret the data.'' In
\corp{Falko} (as in the tiered error annotation of \czesl), TH
is a necessary component of error annotation. In fact, a single
TH is often not enough. Typically, there are two THs in
\corp{Falko}, one for strictly grammatical errors, one for stylistic
issues, idioms etc. One of the arguments for multiple THs is also
valid for successive THs in \czesl
\refS{sec:error:tiers:compromise:why-multiple}:\footnote{However, the
  tiered annotation scheme of \czesl does not support alternative THs
  \refS{sec:error:tiers:compromise:alternative}.}


\begin{quote}
an error-annotated corpus which does not provide target
hypotheses hides an essential step of the analysis – this could lead
to mistakenly assuming that the error annotation which is present in a
corpus is the ‘truth’ or ‘correct analysis’ instead of just one among
many interpretations \citep[142]{Ludeling:Hirschmann:2015}
\end{quote}



% --- TH ---

% Ludeling:

% TH is needed, and often one TH is not enough. 

% ``an error-annotated corpus which does not provide target
% hypotheses hides an essential step of the analysis – this could lead
% to mistakenly assuming that the error annotation which is present in a
% corpus is the ‘truth’ or ‘correct analysis’ instead of just one among
% many interpretations '' \cite[p.~142]{Ludeling:Hirschmann:2015}


% ``Errors cannot be found and analysed without an implicit or explicit
% target hypothesis – it is impossible not to interpret the data.''
% \cite[p.~141]{Ludeling:Hirschmann:2015}

However, in a sentence \e{The girls was laughing\/} the error can be
tagged as an error in agreement without deciding about the grammatical
number and thus without a TH.\footnote{This would only be possible if
  the annotation scheme allowed for the omission of TH and for
  specifying an error in agreement as a relation between the two
  disagreeing words. Technically, the tiered error annotation scheme
  of \czesl can be adapted to accommodate such a solution, although it
  would not be in accordance with the annotation guidelines.} There is
also a similar German example \citep[144,
ex.~7]{Ludeling:Hirschmann:2015} \et{Jeder werden davon
  profitieren}{Jeder wird davon profitieren / Alle werden davon
  profitieren}. Yet in such cases the omission or underspecification of
alternative THs does not eliminate the notion of TH from
error annotation.

It is another example \citep[145, ex.~10]{Ludeling:Hirschmann:2015}
that could raise doubts about the existence of a TH in all cases.  One
of the three possible THs for \e{it sleeps inside everyone from the
  start of being} actually includes an error tag instead of a
correction: \e{it sleeps inside everyone UNIDIOMATIC}. This is still
supposed to be a case of an implicit TH -- the annotator just could
not think about an appropriate idiom. Here we are not convinced that
a TH is present, in any case it has a very ephemeral status.

On the other hand, most errors usually require less expert knowledge
to correct than to classify -- see \nameref{sec:error:implicit}
(\S\ref{sec:error:implicit}). In cases where the TH is obvious from
the incorrect word form and/or the context, the annotator need not
speculate about the learner's intention. In fact,
\citet[141]{Ludeling:Hirschmann:2015} claim that the learner's
intention is not involved in the process of finding a TH in general:

\begin{quote}
It is important to note that the construction of a target hypothesis
makes no assumptions about what a learner wanted to say or should have
said. The analyser cannot know the intentions of the learner. The
‘correct’ version against which a learner utterance is evaluated is
simply a necessary methodological step in identifying an error.
\end{quote}

Based on our experience with all sorts of Czech texts from learners of
various proficiency and L1 background we cannot agree. What else is
there to guide the annotator towards a TH than a guess about the
learner’s intentions?

For a discussion about the practical issue whether TH and error tags
are better annotated in one go or separately see
\refs{sec:process:tiers:manual}.


\section{More than one way to annotate errors in \czesl}
\label{sec:more-than-one}


Error annotation is a crucial component of most \czesl corpora. Our
approach to error annotation is one of the aspects of the corpus
design reflecting the aim to serve many types of users.  Rather than
focusing on a narrow domain of learner language as the annotation
target (such as spelling or lexical errors), the corpus is intended as
open to as many research goals as possible. This is one of the main
reasons why the target hypothesis is aimed at SCz
\index{Standard Czech} and the error
taxonomy is based on the standard linguistic concepts (spelling,
morphology, syntax, semantics, agreement, valency), rather than on
categories rooted in the concepts of interlanguage, communication
strategy or specific research goals.

% A typical learner of Czech makes errors all along the hierarchy of
% theoretically motivated linguistic levels, starting from the level of graphemics up to the level of pragmatics. Our goal is to emend the
% input conservatively, modifying incorrect and inappropriate forms and
% expressions to arrive at a coherent and well-formed result, without any ambition to produce a stylistically optimal solution. Correction is
% possible only when the input is comprehensible. In cases where
% the input or its part is not comprehensible, it is left with a partial or
% even no annotation.

% The error annotation is primarily concerned with the acceptability of
% the grammatical and lexical aspects of the learner’s language in a
% narrow sense. However, we
% anticipate that future projects would annotate the corpus with less
% formal linguistic properties, such as the degree of achievement of a
% communicative goal.


Following the same approach of not aiming at any specific group of users, 
we have designed, tested and used two complementary error annotation schemes, and tested and used another one.
Our first proposal, referred to as the \emph{two-tier annotation scheme}
-- 2T, is based on parallel tiers, representing the source text and supporting successive corrections in two stages:
corrections of spelling and all other corrections
\refS{sec:error:tiers}. 
The two annotation tiers were introduced as a compromise between several theoretically motivated levels and practical concerns about the process of annotation. They enable the annotators to register anomalies in isolated forms separately from the annotation of context-based phenomena but saves them from difficult theoretical dilemmas.

To determine the target hypotheses and to apply the grammar-based error categorization \refS{sec:error:tiers:manual} was the task of human annotators. Some of the texts were annotated independently by two annotators and evaluated
\refS{sec:error:tiers:quality}. 

The tagset used by the annotators, slightly biased towards
morphosyntax, and less detailed than most other error tagsets, was
meant to be complemented by other annotation. The absence of POS
distinctions in the error tags is a way to avoid redundancy in a
corpus which is also annotated by POS tags \refS{sec:ling}. The
lack of a detailed analysis of errors in spelling and morphonology is
to some extent remedied by an automatically applied tagset identifying formal
distinctions between the source forms and their corrections
\refS{sec:error:tiers:formal}.

Our second proposal, the \emph{multidimensional} annotation scheme -- MD,
was developed to complement the 2T scheme by filling the gaps in the
categorization of errors in spelling, morphonology and morphology,
while allowing for alternative interpretations of a single error, \eg 
as an error which could be explained as an issue of spelling,
morphonology, morphology or morphosyntax \refS{sec:error:md}.

The motivation for using \emph{implicit annotation}
\refS{sec:error:implicit}, \ie corrections without error tags, is
twofold. Firstly, the full-fledged manual 2T or MD annotation requires
a well-trained annotator and more time for the same amount of
text. Eliminating the error tagging task makes the perspective to
hand-annotate all currently available and new \czesl texts realistic, while leaving open the option to
assign error tags later. Secondly, corrections can be assigned to specific error
interpretation levels, corresponding to the tiers in the 2T scheme, or
to a more sophisticated system of linguistic domains, as in the MD
scheme. Thus, the three error annotation schemes are compatible and
complementary. In fact, the three schemes can be implemented in a single
corpus.



\input{05-error-1-intro.tex}



\input{05-error-2-tiers-0.tex}
\input{05-error-2-tiers-1-manual-tags.tex}
\input{05-error-2-tiers-2-manual-quality.tex}
\input{05-error-2-tiers-3-formal-tags.tex}
\input{05-error-3-implicit.tex}
\input{05-error-4-multidim.tex}
