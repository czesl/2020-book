\subsection{Automatic error annotation}
\label{sec:process:md:auto-error}

\index{annotation!automatic}
\index{annotation!error}
Another part of the program for pre-processing learner texts before the manual error annotation is the automatic detection of errors (\ie of differences between the forms on T0 and T2) and preliminary determination of their types.
It is performed for corrected words: first the strings are compared and their differences are located, then the domain and type of error (``feature'' -- see \refs{sec:error:md}) is identified.
The program primarily classifies the detected errors into two domains: \tg{ORT} (orthographic errors) and \tg{MPHON} (morphonological errors, \ie errors affecting pronunciation).
In a limited number of cases, it also identifies other errors, such as \tg{CHOICE} (a lexical error).
If the detected error type belongs to both the \tg{ORT} and \tg{MPHON} domains,
only one of the domain (and one error type) is chosen and the
annotators are instructed not to add the second error type,
which will be filled in automatically after manual annotation.

The program compares the original and the corrected word forms from the beginning, character by character.
If the forms begin differently (for example,
if a prefix is missing in the source form: \eet{rozumívat}{dorozumívat}{understand},
or the prefix for the original and corrected form is different: \eet{skončit}{dokončit}{finish}), the program looks for the first position where the forms match (characters could be substituted, omitted, inserted or transposed, or the whole word may be different).
To identify positions where the two forms match, a complete match is not required. It is sufficient if the characters are similar (graphically or in pronunciation):
characters differing in upper/lower case, diacritics  (\et{š}{s}, \et{á}{a}), voicing (\et{s}{z}), \e{i} and \e{y} are considered similar.
Similar partial matches are considered as half-errors.
To consider a match of form portions, a half-matching character must be followed by another half-matching or a full-matching character.
If the words are successfully aligned, all differences are marked.

Using simple rules, these differences might be split into several individual errors.
For example, if the source word form \e{kultůru} is corrected as \eec{kultuře}{culture}{DAT/LOC}, the difference is at the fifth, sixth and seventh character: \et{ůru}{uře}.
However, the program determines that these are three separate, unrelated errors:
(i) an error in the vowel quantity of \et{ů}{u} (\tg{MPHON:QUANT} and \tg{ORT:U});
(ii) an error in a missing diacritics \et{r}{ř}, resulting in the
failure to “soften” the consonant \e{r} (\tg{MPHON:SOFT} and \tg{ORT:DIA}); and
(iii) an error where the ending \e{-u} is incorrectly replaced with \e{-e} (\tg{MPHON:ALT}).
The latter two errors are probably related to the use of an existing
but wrong ending, but the program cannot determine this yet and such
errors have to be marked manually.
The program uses schemata (rules) to identify several dozens of error subtypes (mainly in the domain of orthographic and morphonological errors).
Some schemata are very simple (such as labeling errors in uppercase/lowercase letters, missing/inappropriate quantity),
others are more complex, dependent on the phonetic environment, stem of the governing word, etc.\ (palatalization; decision whether to use error mark \tg{CHOICE} etc.).
\sasa{nerozumím: "stem of the controlling word"}

\subsection{Experiments with automatic identification of errors in
  inflection}

\index{annotation!automatic}
\index{error!inflection}

We experimented also with automatic identification of errors in inflection, which (if reliable enough) would significantly reduce the workload on manual annotators.
The experiments had promising results, but we decided not to implement
this module before the manual annotation would provide enough data to test it automatically.
The following text describes this experiment and its (partial) results.

The experiment targets those words in the source text whose corrected form was identified as an inflectional word.
Morphemic analysis, described in \refs{sec:process:md:morphemic}, was used to split both the source and the corrected word forms into a stem and an inflectional suffix (and sometimes prefix).

For example, if the incorrect source form is \e{stromom}
‘tree’ and the TH is \e{strom} with an empty
inflectional suffix, the system does not compare only the two last
characters of both words (which are identical by chance), but compares the entire stems and determines
that the suffix of the original word is \e{-om} (\e{strom$\vert$om}).
Using the stems and inflectional affixes for both the original and the TH forms, a
two-dimensional comparison of the stems and the 
affixes is then performed. If the stems (original and TH) differ, two
facts are checked: whether there are any minor errors in the stem (orthographic,
phonological), and whether the source stem is an
allomorph of the stem of the TH form, as in \eet{v Prahe}{v Praze}{in
Prague}, where the original stem \e{Prah} (incompatible with the 
\e{-e} suffix) is used to form other (correct) forms of the same lemma,
\eg \e{Prah$\vert$a, Prah$\vert$y} etc.

If the affixes differ, they are also checked for minor changes
(orthography, \eg diacritics). Another check tests whether the
incorrect affix is used within the given paradigm for other
morphosyntactic properties, or whether the affix is used with other
paradigms to express the same morphosyntactic properties.
The observed differences correspond roughly to
the proposed error classification scheme: all errors in orthography
and most of the errors in morphonology can be identified
automatically.
Incorrect affixes indicate an error in morphology; if
the incorrect ending is an existing one, expressing the same
morphosyntactic properties, it may be an error only in morphology,
otherwise it has to be seen as a possible error in syntax as well. If
the original word is correct and has the same morphosyntactic
properties as the TH word, but the lemma is different, the error
may belong to the lexical domain (except for function words).
The relationship between the automatic classification and the
classification into error domains is not straightforward. A
manual test on a sample of 500 learner errors shows that the
approach is reliable with more than 90\% of categories determined
correctly. As the system is rule-based, it can be fine-tuned by
modifying the rules.

% \subsubsection{Results of automatic classification of errors in the inflection of nouns}
We tested the rule-based system on nouns in the \corp{CzeSL-man}
corpus. Ill-formed nouns were identified as such using the
disambiguated POS tags for corresponding corrected forms on T2. The
texts were divided by language proficiency of the authors in terms of
CEFR.
%\footnote{The language proficiency scale is specified according to
%the Common European Framework of Reference for Languages (CEFR), see
%\url{https://www.coe.int/en/web/common-european-framework-reference-languages}.}
The
levels are not evenly distributed, as shown in \autoref{tab:Data:distribution}.

\begin{table}
  \centering
  \begin{footnotesize}
%    \begin{tabular}{p{0.32\textwidth}p{0.10\textwidth}p{0.10\textwidth}p{0.10\textwidth}p{0.10\textwidth}p{0.10\textwidth}p{0.10\textwidth}}
\begin{tabular}{lrrrrrr}
 & \cellalign{c}{A1} & \cellalign{c}{A2} & \cellalign{c}{B1} & \cellalign{c}{B2} & \cellalign{c}{C1} & \cellalign{c}{Total}\\ \hline
Number of tokens & 6,961 & 42,252 & 39,987 & 28,182 & 5,522 & 122,904\\
Percentage of the data & 5.66 & 34.38 & 32.54 & 22.93 & 4.49 & 100.00\\
  \end{tabular}
  \end{footnotesize}
\caption{Data distribution by language proficiency}
\label{tab:Data:distribution}
\end{table}

We performed two analyses of nouns in the \corp{CzeSL-man} corpus: one more
general, determining the proportion of incorrect nouns in the corpus,
one detailed, focused only on errors in inflection suffixes of nouns.

\autoref{tab:Proportion-of-ci-nouns} shows the proportion of correct
nouns, nouns with an incorrect suffix (\eet{jeskyne}{jeskyně}{cave}), with an incorrect stem and a correct
suffix (\eetc{Prahe}{Praze}{Prague}{dat/loc}), with both stem and suffix
incorrect (\eetc{delki}{délky}{length}{gen}), and impossible to split
automatically (\eetc{těmy}{tématu}{topic}{gen}).

\begin{table}
  \centering
  \begin{footnotesize}
%    \begin{tabular}{p{0.2\textwidth}p{0.08\textwidth}p{0.08\textwidth}p{0.08\textwidth}p{0.08\textwidth}p{0.08\textwidth}p{0.08\textwidth}}
\begin{tabular}{lr@{.}lr@{.}lr@{.}lr@{.}lr@{.}lr@{.}l}
 & \multicolumn{2}{c}{A1} & \multicolumn{2}{c}{A2} & \multicolumn{2}{c}{B1} & \multicolumn{2}{c}{B2} & \multicolumn{2}{c}{C1} & \multicolumn{2}{c}{Total}\\ \hline
Correct & 61&32 & 68&15 & 77&45 & 78&01 & 95&09 & 74&25\\
Incorrect ending & 9&10 & 10&68 & 6&30 & 6&30 & 0&97 & 7&72\\
Incorrect stem & 18&91 & 14&20 & 11&18 & 11&23 & 3&17 & 12&31\\
Incorrect whole & 19&77 & 17&65 & 11&37 & 10&76 & 1&74 & 13&44\\
Total & 100&00 & 100&00 & 100&00 & 100&00 & 100&00 & 100&00\\
    \end{tabular}
  \end{footnotesize}
\caption{Proportion of correct and incorrect nouns by proficiency levels}
\label{tab:Proportion-of-ci-nouns}
\end{table}


% \begin{tabular}{r@{.}ll}
% \multicolumn{2}{c}{tady} \\
%   3&14159 & x \\         
%   16&2 & x \\         
%   123&456 & x \\ 
% \end{tabular}     

The proportion of correct nouns increases with the proficiency level,
but there is little change between B1 and B2. On the other hand, there is an unexpectedly
large difference between B2 and C1 in the proportion of correct
nouns. The highest proportion of incorrect suffixes is in the A2 level
texts.

We analyzed in more detail the errors in nominal suffixes: all
nouns with either a correct stem, or with minor changes
compared with the TH were examined. Two parameters were
observed: whether the error in the suffix can be an error in orthography, and whether the suffix is an
existing Czech inflectional suffix used either to express the same case, number and
gender in other paradigms, or is used in the same paradigm to express
other morphosyntactic properties.
\autoref{tab:Proportion-of-etypes} shows the analysis of errors in
nouns in \czesl. Six subtypes of nouns with incorrect suffix were registered:
\begin{description}
\item[Other paradigms:] a suffix used in other
  paradigms (\eetc{Úvalách}{Úvalech}{Úvaly}{loc}, a place name); likely syntactically correct
\item[Other paradigms \& spelling:] a suffix 
  used in other paradigms and with a spelling error at the same
  time (\eetc{Prázě}{Praze}{Prague}{dat/loc})
\item[Paradigm:] a suffix used inside the paradigm for
  other morphosyntactic properties (\eet{na
    procházka\e{\cat{*nom}}}{procházku\e{\cat{acc}}}{on/for a walk});
  this is probably an error in morphosyntax
\item[Paradigm \& spelling:] as above, with a spelling error at the same time (\eet{lidi}{lidí}{people})
\item[Spelling:] only a spelling error, none of the above (\eet{pracé}{práce}{work})
\item[Other:] all other instances
\end{description}

\begin{table}
  \centering
  \begin{footnotesize}
%    \begin{tabular}{p{0.32\textwidth}p{0.10\textwidth}p{0.10\textwidth}p{0.10\textwidth}p{0.10\textwidth}p{0.10\textwidth}p{0.10\textwidth}}
\begin{tabular}{lr@{.}lr@{.}lr@{.}lr@{.}lr@{.}lr@{.}l}
 & \multicolumn{2}{c}{A1} & \multicolumn{2}{c}{A2} & \multicolumn{2}{c}{B1} & \multicolumn{2}{c}{B2} & \multicolumn{2}{c}{C1} & \multicolumn{2}{c}{Total}\\ \hline
Other paradigm & 12&19 & 14&90 & 14&16 & 18&97 & 16&20 & 15&23\\
Other paradigm \& spelling & 8&87 & 4&51 & 7&08 & 6&91 & 7&82 & 6&83\\
Paradigm & 19&38 & 30&77 & 22&83 & 24&03 & 24&02 & 25&34\\
Paradigm \& spelling & 4&03 & 3&24 & 5&25 & 5&14 & 11&73 & 4&40\\
Spelling & 7&65 & 2&94 & 3&65 & 7&00 & 7&82 & 4&06\\
Other & 47&88 & 43&64 & 47&03 & 37&94 & 32&40 & 44&14\\
Total & 100&00 & 100&00 & 100&00 & 100&00 & 100&00 & 100&00\\
    \end{tabular}
  \end{footnotesize}
\caption{Proportion of types of errors in endings of nouns}
\label{tab:Proportion-of-etypes}
\end{table}

We observe a steady decrease of “Other” errors, and an increase in the
proportion of orthographic errors with language proficiency levels
(the authors with a higher proficiency make less errors in
general, but keep omitting diacritics).
The system allows also for the analysis of
individual suffixes: we observed, for example, that suffixes with high
ambiguity such as \e{-e}, \e{-i}, \e{-í} are more prone to
errors (already noted by \cite[220]{Hudouskova:2014}).

% \evb{\cite{Jelinek:2017}}
