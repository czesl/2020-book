\subsection{Formal tags}
\label{sec:error:tiers:formal}

\index{error!formal|textbf}
\index{annotation!automatic}

\subsubsection{Automatic extension and modification of error annotation}
 

From the first designs of the 2T \czesl error annotation, it was assumed that, following the manual annotation, some types of errors would be annotated automatically. The automatic annotation would add a different point of view on the errors, based on a simple comparison of the source and corrected words. Manual annotation of errors on T1 is relatively simple, and the automatic annotation of errors can give the user much more detailed information about the error and sometimes even the most likely cause of the error. For example, the word form \e{hřipku}, used instead of the correct form \eec{chřipku}{flu}{sg.acc}, is probably not a case of a character omission, but an error in voicing (\tg{formVcd1}) -- the character \e{h} is prototypically used for the voiced phoneme \phonology{H}, while the digraph \e{ch} for the voiceless phoneme \phonology{H} (\phonetics{h} and \phonetics{x} form a voicing pair in Czech). The manual annotation of this error only assigns a simple distinction that the word contains an unspecified error in the stem (flective base of the word): \tg{incorBase}.

\index{error!spelling} \index{error!pronunciation}
The automatic error annotation in \czesl on T1 addresses errors of a formal nature,
\ie (broadly) orthographic errors, such as incorrect capitalization, wrong use of diacritics or wrong choice of the characters \e{i} {\lrarrow} {y}, and
errors reflecting wrong pronunciation, such as voicing or (so called) hard and soft consonants, \eg \e{d} {\lrarrow} \e{ď}:
the character \e{ď} (\e{d} with a caron) marks in Czech the phoneme \phonology{\phDj} (voiced palatal plosive).
Apart from formal error detection, the automatic error annotation refines the error tagging of errors in word boundaries (incorrect division or joining of word forms).

The T1-formal errors make no distinction whether the error occurs in the stem or in the inflectional affix;
this distinction is assumed to come from the manual annotation: \tg{incorBase} or \tg{incorInfl}.
The reason is that 
morpheme boundaries are often blurred in Czech (as in many other inflective languages) and it is not trivial to distinguish errors in inflection from errors in the stem automatically. 

Some T2 errors are annotated automatically to some extent -- the annotators mark errors with a more general tag and an automatic procedure automatically assigns a detailed tag.

The automatic annotation of T2 uses only a limited amount of information about morphological tags and lemmas.
We do not attempt to identify the cause of the error. A form may be incorrect due to an incorrectly applied morphological paradigm or due to an incorrect syntax structure of the sentence. Instead, we only further specify or complement manually assigned tags.
For example, an error in a periphrastic verb form is manually corrected and labeled with a general error tag, the automatic annotation then adds a more detailed tag distinguishing past-tense errors and modal-verb errors.

\subsubsection{Automatic detection of formal errors on T1}
\label{sec:error:formal:detection}

% \sasa{dal bych všude prézens, takhle to vypadá jako nějaká zpráva, ne?}

% \sasa{Tohle myslím nemá paralelu v části o procesu}
% \tomas{Paralela bude co nevidět!}

Automatic extension of the error annotation on T1 is performed for those T0 forms that are corrected on T1. It is based on a comparison of the source T0 form and the corrected T1 form.  The result is the assignment of formal error tags: broadly orthographic errors, pronunciation-related errors. All such tags are prefixed by the \tg{form}{\ldots} string. Moreover, manually annotated errors in word boundaries (\tg{wbd}) are specified as words incorrectly joined or split. For a list of all formal error tags on T1 see \autoref{tbl:error:formal:T1} and \autoref{tbl:error:formal:T1b} (page \pageref{tbl:error:formal:T1}--\pageref{tbl:error:formal:T1b}).

The algorithm identifies individual differences between the corresponding T0 and T1 forms (\eetc{delamé}{děláme}{make}{1pl} contains three individual differences: \et{e}{ě}, \et{a}{á}, \et{é}{e}) and assigns an error tag to each difference.
The 2T \czesl annotation scheme does not track the exact location of error, therefore error tags are assigned to the whole word.
If there are multiple errors, the word is assigned multiple tags (\et{delamé}{děláme}: \tg{formCaron0}|\tg{formQuant0}|\tg{formQuant1}).
A single difference can also be assigned multiple error tags
(the difference \et{i}{ý} in \eet{úteri}{úterý}{Tuesday} is assigned \tg{formY0}|\tg{formQuant0} to mark the confusion of \et{i}{y} and a missing accent \et{y}{ý}).

For the formal tags, we use the following convention: they end with \tg{0} for incorrectly missing phenomena and \tg{1} for incorrectly realized phenomena.
For example, incorrect spelling of words due to voicing assimilation can be marked either with \tg{formVcd0} or with \tg{formVcd1}:
\eetc{pohátková}{pohádková}{fairytale}{adj} uses voiceless \e{t} instead of the correct voiced \e{d} and is therefore marked with \tg{formVcd0},
while \eet{svadba}{svatba}{wedding} uses voiced \e{d} instead of the correct voiceless \e{t} and is therefore marked with \tg{formVcd1}.

For expository reasons, we classify automatically assigned formal tags into two groups: (broadly) orthographic errors, and formal errors affecting pronunciation. Orthographic errors concern misspellings that do not affect pronunciation of the misspelled form by native speakers. They are often the types of errors that even native speakers commonly make in their texts. Formal errors affecting pronunciation include errors in which there would be a noticeable difference in pronunciation between the original and the corrected form. The most common errors of this type include missing diacritics indicating the quantity of vocals or softening of consonants. We list only errors that actually occurred in student corpora; other errors are handled by the algorithm as well, but they are not present in the examined texts.


\subsubsection{Formal orthographic errors}
\index{error!spelling|textbf}

Automatically identified orthographic errors include errors in capitalization. Czech uses capital letters to mark sentence beginnings and proper names. Rarely, they are also used for emphasis of certain words.
The error tag \tg{formCap1} marks words that should be written with a lower-case letter, but are capitalized
(\ee{ona Rozumí {\arrow} rozumí trochu český}{she Understands {\arrow} understands little Czech}).
Missing capitalization is labeled with \tg{formCap0} (\ee{Líbí se mi praha {\arrow} Praha}{I like prague {\arrow} Prague}).

Another group of orthographic errors are errors in the spelling of vowel groups with \e{ě}.
In Czech, some phoneme sequences can be spelled in two ways:
\phonology{bjE} as \e{bě} or \e{bje}, \phonology{pjE} as \e{pě} or \e{pje}, \phonology{vjE} as \e{vě} or \e{vje}, \phonology{m\phNj E} as \e{mě} or \e{mně}
(the spelling depends on the origin of the word, for example, \e{bje}, \e{pje}, \e{vje} are used across a morpheme boundary).
We distinguish formal errors in the spelling of the phonemes \phonology{jE} (\eet{rozbjehl}{rozběhl}{started to run}: \tg{formJe1}) and
errors in the spelling of \phonology{mɲE} (\eet{vzpoměla}{vzpomněla}{remembered} \tg{formMne0}, \eet{mněla}{měla}{had} \tg{formMne1}).

A similar phenomenon applies to the orthographic notation of phonetic groups with palatal plosives \phonology{c} (\e{ť}), \phonology{\phDj} (\e{ď}) and palatal nasale \phonology{\phNj} (\e{ň}).
These phonemes are spelled with a diacritic mark caron (wedge, hacek), but in a combination with the vowel \e{e}, they are spelled as \e{tě}, \e{dě}, \e{ně} and in a combination with the vowel \e{i}, \e{í} as \e{ti}, \e{di}, \e{ni}, and \e{tí}, \e{dí}, \e{ní}, respectively.
The spelling with a caron on the consonant (\eet{kuchyňe}{kuchyně}{kitchen}) is wrong and is labeled with the error tag \tg{formDtn}.

%\bara{Popsáno už na začátku 5.2.4.5} \sasa{Něco jsem našel v \refs{sec:error:tiers:formal:ext}, ale tam to patří a sem taky. Možná je to jinde?}

Another error concerns the marking of length of the vowel \e{u}.
In Czech, two variants with the same pronunciation are used: \e{ú} (\e{u} with an acute diacritic) and \e{ů} (\e{u} with a ring diacritic).
Simplifying somewhat, \e{ú} is used word and morpheme initially, and \e{ů} otherwise.
Mistakes of this nature (\eet{dúm}{dům}{house}, \eet{ůkol}{úkol}{task}) are labeled with \tg{formDiaU}.

Non-native speakers sometimes make mistakes in spelling of the vowel \e{i} followed by a syllable boundary and another vowel.
In original Czech words, the vowels are always separated by \e{j}, both in spelling and pronunciation.
Borrowed words are often spelled without \e{j}, even though the glide is present in a correct pronunciation \e{j} (\ee{piano}{piano} is orthoepically pronounced as \e{pijáno}). Non-native speakers sometimes confuse this rule:
we label with \tg{formEpentJ0} an incorrect omission of \e{j} 
(\eet{přiela}{přijela}{arrived}, \eetc{žiou}{žijou}{live}{3pl}, \eetc{pieme}{pijeme}{drink}{1pl}),
and with \tg{formEpentJ1} a superfluous \e{j} (\eetc{fotografijemi}{fotografiemi}{photos}{inst}, \eet{studijum}{studium}{study}, \eet{dijamant}{diamant}{diamant}).


\subsubsection{Formal errors sometimes influencing pronunciation}
\index{error!spelling} \index{error!pronunciation}

Several automatically identified types of formal errors are at the boundary between orthographic errors and errors affecting pronunciation.
In some contexts, the error may affect pronunciation, in others it is purely orthographic, but we did not see any benefit in introducing another group of errors.

The confusion of the homophonous letters \e{i}$\leftrightarrow$\e{y}, which are both used to spell the vowel \phonetics{I}, is labeled
with \tg{formY0} when replacing \e{y} with an incorrect \e{i} (\eet{kdiž}{když}{when}) or \e{ý} with \e{í} (\eet{svími}{svými}{self's}), or
with \tg{formY1} when \e{i} is replaced with an incorrect \e{y} (\eetc{hystorek}{historek}{stories}{gen}), or \e{í} with \e{ý} (\eet{ostatným}{ostatním}{others}).
This error affects pronunciation in combination with the characters \e{t}, \e{d} and \e{n}, in the other cases it is a purely orthographic error.
In the words of Czech origin,  \e{y} never follows \e{č}, \e{ř}, \e{š}, \e{ž}, \e{j}, and \e{i} never follows \e{h}, \e{ch}, \e{k}, \e{r}.
After some characters (\e{b}, \e{f}, \e{l}, \e{m}, \e{p}, \e{s}, \e{v}, \e{z}), both \e{i} and \e{y} are commonly used in Czech;
in some cases, the use of the character distinguishes the meaning of homophones (\ee{bil}{beated} vs.\ \ee{byl}{was}).

The pronunciation of the word-initial character \e{j} preceding certain consonants (\e{s}, \e{m}, \e{d}) is optional (the prescribed pronunciation of \ee{jsem}{am} is \phonetics{sEm}).
In colloquial use, such words are often written without the initial \e{j}, even though it often distinguishes meaning (\ee{jsem}{am} vs.\ \ee{sem}{here}). Such words in learners texts are given the error tag \tg{formProtJ0}. Similarly, words with an incorrectly added initial \e{j} (\eetc{jsi}{si}{\textsc{refl}}{dat}) are labeled with \tg{formProtJ1}. In these cases (significantly predominant in terms of frequency), this error is orthographic, however, the error tag is used for any missing or redundant \e{j} before any consonant at the beginning of a word. Therefore, it is also used for errors such as  \eetc{jšla}{šla}{went}{fem} where the change of pronunciation is possible.

Errors at the border between orthographic errors and errors affecting pronunciation also include errors in voicing. In Czech, consonants in clusters assimilate in voicing, but their spelling preserves voicing based on their morphological and phonological structure (the word \ee{fotbalista}{soccer player} is pronounced as a \phonetics{fodbalista} due to voicing assimilation of \phonology{t} with the voiced \phonology{b}).
Errors caused by “phonetic” spelling are labeled as \tg{formVcd0} (replacing a voiced character with its voiceless counterpart,
\eg \eet{skouší}{zkouší}{tries}) or \tg{formVcd1} (replacing a voiceless character with its voiced counterpart \eet{fodbalista}{fotbalista}{soccer player}).
Czech voiced obstruents are devoiced word-finally, but they also preserve their voicing in spelling.
Incorrect use of voiceless consonants in such cases is labeled with \tg{formVcdFin0} (\eet{kdyš}{když}{when}).
Prepositions ending in a voiceless consonant optionally assimilate with the voiced consonant of the following word (\ee{přes hodinu}{over an hour} is pronounced as \e{přezhodinu} or \e{přeshodinu}), but their spelling does not change either. The errors such as \eet{přez}{přes}{over} are labeled as \tg{formVcdFin1}. However, we use the \tg{formVcdFin1} tag for any incorrect use of a word-final voiced consonants, even for those that are not related to preposition voicing assimilation, including errors that a native speaker is unlikely to make (\eet{svěd}{svět}{world}).
The remaining errors caused by an incorrect use of voiced consonants instead of voiceless ones and vice versa, which are not related to consonant cluster voicing assimilation and are not word final, are labeled as  \tg{formVcd} (\eet{přehod}{přechod}{crossing}, \eetc{sůstala}{zůstala}{stayed}{fem}).

Sometimes, under the influence of the spelling rules in other European languages,
\e{j} is incorrectly replaced with \e{y} (\eet{yá}{já}{I}, \eetc{žiyu}{žiju}{live}{1sg}, \tg{formYJ0}), and
\e{k} with \e{c} (\eetc{clientovi}{klientovi}{client}{sg.dat}, \eetc{culturu}{kulturu}{culture}{sg.acc} \tg{formCK0}).
The words would be pronounced incorrectly if we followed the rules of Czech pronunciation, but it is likely that the author pronounces it correctly and just used an incorrect spelling.
In Czech, the letter \e{y} is always pronounced as the vowel \phonetics{I}, never as the glide \phonetics{j}.
The character \e{c} in Czech words (original Czech words and not recent borrowings) is always pronounced as \phonology{\phTs} (voiceless alveolar affricate).
It is used for \phonology{k} only in recently borrowed words.

Another phenomenon that includes both orthographic and pronunciation changes involves double phonemes.
In Czech, double consonants are sometimes pronounced as two phones and sometimes as one.
Double pronunciation is used especially for vowels separated by a morpheme boundary (\ee{poloostrov}{peninsula}, \eec{individuu}{individual}{dat}).
Often they are separated by a glottal stop (\phonetics{P}).
Double pronunciation of consonants is also sometimes used to distinguish meaning (\ee{racci}{seaguls} vs.\ \ee{raci}{crayfish}).
Two identical consonants when one is in a prefix and another in a root are also often pronounced as two (\ee{oddálit}{postpone}, \ee{dvojjazyčný}{bilingual}), but not always (\ee{leccos}{all sorts of things}).
Double pronunciation across a root-suffix boundary is rather rare (\ee{vyšší}{taller}, \ee{činnost}{activity}, \ee{babiččin}{grandma’s}).
Errors in spelling of double and single letters are labeled with \tg{formGemin}:
\tg{formGemin0} is used for characters that should be doubled but are not (\eet{povinost}{povinnost}{duty}, \eet{polostrov}{poloostrov}{peninsula}), and
\tg{formGemin1} is used for consonants that are doubled but should not be (\eetc{sobbota}{sobota}{Saturday}{acc}, \eet{rukoppis}{rukopis}{manuscript}).
The designation of error is slightly misleading as Czech does not have a real gemination.


\subsubsection{Formal errors influencing pronunciation}
\index{error!pronunciation|textbf}
\index{error!diacritics}

One of the types of automatically identified formal errors affecting pronunciation (by native speakers) are errors caused by inappropriate writing of diacritics.
Czech, has three diacritical marks: caron (wedge, hacek), acute accent and ring.
Acute accent and ring indicate a vowel is long (\eec{sila}{silo}{pl} vs.{} \ee{síla}{force}, \eec{pul}{halve}{imper} vs.{} \ee{půl}{half}).
Caron indicates so-called softening of consonants, \ie shifting the place of articulation of alveolar consonant backwards:
to postalveolar as in \e{z} \phonology{z} {\arrow}  \e{ž} \phonology{Z} or to palatal as in \e{d} \phonology{d} {\arrow} \e{ď} \phonology{\phDj}.
The pronunciation of the \e{ě} character depends on the previous consonant.
Errors in vowel quantity are labeled with \tg{formQuant}:
missing diacritics with \tg{formQuant0} (\eet{libí}{líbí}{likes}),
extra diacritics with \tg{formQuant1} (\eet{výprávěl}{vyprávěl}{narrated}).
A missing caron is labeled with \tg{formCaron0} (\eetc{pojd}{pojď}{come}{imper}, \eet{neco}{něco}{something}),
a superfluous caron is labeled with \tg{formCaron1} (\eet{kteřých}{kterých}{which}; \eet{věnkově}{venkově}{country}).
The incorrect use of caron instead of acute or vice versa above the character \e{e} is labeled with \tg{formDiaE} (\eet{možně}{možné}{possible}, \eet{obchodé}{obchodě}{shop}).


\index{palatalization}
Palatalization is a historically motivated consonant alternation.
Velar and glottal consonants (\e{k},  \e{h} \phonology{H}, \e{ch} \phonology{x}, and \e{g} in borrowed words) change when followed by a morpheme originally containing the vowel yat, typically realized as \e{e}, \e{ě} or \e{í} in modern Czech: \e{k} {\arrow} \e{c} \phonology{\phTs}, \e{h} \phonology{H} {\arrow} \e{z}, \e{ch} \phonology{x} {\arrow} \e{š} \phonology{S}, \e{g} {\arrow} \e{z}.
Errors in palatalization occur most often in the declension of nouns whose stem ends in one of the above consonants.
For example, the paradigm \ee{žena}{woman} has the ending \e{-ě} in dative and local singular.
The noun \ee{řeka}{river} belonging to this paradigm has the form \e{řece} (stem-final \e{k} changes to \e{c}, and the spelling of the ending \e{-ě} changes to \e{-e} in these cases).
Missing palatalization is labeled with \tg{formPalat0} whether the author uses \e{-ě} (\et{řekě}{řece}) or \e{-e} (\et{řeke}{řece}).
Non-native speakers sometimes make also the opposite error: applying palatalization in places where it should not be: \eec{koníčkem}{hobby}{inst} has the ending \e{-em} that historically does not contain yat and thus there is no palatalization.
All cases of unjustified palatalization are assigned the error tag \tg{formPalat1} (\eetc{koníčcem}{koníčkem}{hobby}{inst}, \eetc{pracovnícem}{pracovníkem}{worker}{inst}).

\index{epenthesis}

The following formal error also has a historical connection. 
Yers, 
%(ъ and ь)\bara{Nevysází se dobře.}, 
Proto-Slavic vowels, disappeared in Old Czech, some transforming into \e{-e-} and some disappearing completely.
This is the cause of alternating forms with and without \e{-e-} (\eec{pátek}{Friday}{nom} -- \eec{pátku}{Friday}{gen}).
Synchronically, this is manifested as an epenthesised \e{-e-} making it easier to pronounce some words that would otherwise contain a consonant cluster both morpheme internally (\eec{kra}{iceberg}{nom} -- \eec{ker}{icebergs}{gen} not \e{kr}) and across morpheme boundaries (\e{roz} + \e{brát} -- {rozebrat} `take appart').
Forms with a missing \e{-e-} are labeled with \tg{formEpentE0} error (\eetc{odbereme}{odebereme}{remove}{1pl}).
However, the error is defined broadly: it applies to any missing \e{-e-} between two consonants,
and most occurrences of this error are thus only loosely related to the original \e{-e-} epenthesis:
\eet{odpoldne}{odpoledne}{afternoon}, \eetc{přijla}{přijela}{arrived}{fem.sg}, \eetc{telvizi}{televizi}{TV}{acc}.
An extra \e{-e-} between two consonants is labeled with \tg{formEpentE1}.
This is used both in cases where \e{-e-} occurs in other forms of the paradigm
(\eet{dáreky}{dárky}{presents} \cf \ee{dárek}{present}, \eetc{páteku}{pátku}{Friday}{gen}, \cf \eec{pátek}{Friday}{nom}),
and in cases that are due to pronunciation difficulty of consonant clusters (\eet{jemenuje}{jmenuje}{is named}, \eet{čtvertek}{čtvrtek}{Thursday}).


\index{Colloquial Czech}
\index{prothesis}
In spoken Czech, in Bohemia and Central Moravia, word-initial \e{o-} is often preceded with a prothetic \e{v}. For example, some speakers pronounce the word \ee{okno}{window} as \e{vokno} and sometimes they even write it in that nonstandard way (but the phenomenon is probably currently declining).
In the \czesl project, we evaluate the written text against the rules of SCz, so we label occurrences of prothetic \e{v} as errors with the \tg{formProtV1} tag:
\eet{vobčas}{občas}{sometimes}, \eet{vopravdu}{opravdu}{really}.

During the evolution of Czech from Proto-Slavic, the original phoneme \e{g} changed into \e{h}. However, this process did not occur in most other Slavic languages. This is a cause for another type of error when non-native speakers mostly of Slavic origin confuse the letters \e{g} and \e{h}. The incorrect use of the letter \e{g} in place of \e{h} is labeled with \tg{formGH0}: \eet{glavní}{hlavní}{main}, \eet{mnogo}{mnoho}{many}, \eetc{gasič}{hasič}{firefighter}{inst}.
Czech uses \e{g} in newly borrowed words.
An incorrect replacement of such \e{g} with \e{h} is labeled with \tg{formGH1}:
\eetc{ciharetu}{cigaretu}{cigarette}{acc}, \eetc{prohramů}{programů}{programs}{gen}, \eetc{hrafička}{grafička}{graphic artist}{fem}.

\index{error!metathesis}
Character metathesis is a relatively common type of error for both non-native and native speakers.
We automatically identify two types of metathesis:
swapping adjacent characters (\eet{sulnce}{slunce}{sun}, \eet{dobrodružtsví}{dobrodružství}{adventure}), and
swapping two characters separated by another character (\eet{provůdce}{průvodce}{guide}, \eetc{ojelů}{olejů}{oil}{pl.gen}, \eetc{zicích}{cizích}{foreign}{pl.gen}).
These errors are labeled with \tg{formMeta} error tag.


\subsubsection{Other types of errors}

The variability of errors in the texts of non-native speakers is too great, so it is not possible to systematically handle all cases. In this section we focus on automatically assigned tags that cannot be classified into any of the above categories. The tags attempt to provide at least some information about the nature of the difference in the original and corrected word. They almost always affect pronunciation.

There are three error tags for labeling single-character mistakes that cannot be classified with any of the more descriptive tags above.
The \tg{formSingCh} tag indicates cases where one character is replaced by another:
\eetc{specifi\ebc{s}ké}{specifi\ebc{c}ké}{specific}{neut}, \eet{exist\ebc{i}je}{exist\ebc{u}je}{exists}, \eetc{o\ebc{f}jevit}{o\ebc{b}jevit}{appear}{inf}.
The \tg{formMissChar} tag is assigned to cases where a single character is missing:
\eetc{učiteka}{učite\ebc{l}ka}{teacher}{fem}, \eetc{výjmečnému}{výj\ebc{i}mečnému}{exceptional}{masc.dat}, \eet{zbudil}{\ebc{v}zbudil}{woke up}.
The \tg{formRedunChar} tag is used in cases with an extra character:
\eet{usmr\ebc{d}cení}{usmrcení}{killing}, \eet{pr\ebc{i}vního}{prvního}{first}, \eet{ku\ebc{g}ličky}{kuličky}{marbles}.

Another error tag marks mistakes due to a missing or extra prefix. Therefore, the error tag expresses that there are one or several characters missing or extra word-initially and that the characters are equal to one of the commonly used Czech prefixes.
Errors where the prefix is missing in the original word are labeled with \tg{formPre0}:
\eetc{hledu}{\ebc{po}hledu}{view}{gen}, \eet{žaduje}{\ebc{vy}žaduje}{requires}, \eetc{znamil}{\ebc{se}známil}{introduced}{masc.sg}.
Words with extra prefixes in the original are labeled with \tg{formPre1}:
\eetc{\ebc{po}jet}{jet}{drive}{inf}, \eetc{\ebc{pře}začít}{začít}{start}{inf}, \eetc{\ebc{po}trávíme}{trávíme}{spend}{1pl}.
In some cases, the tag is also assigned to incorrectly fused words that were manually annotated in a wrong way:
the incorrectly fused word \e{semnou}, corrected to \ee{se mnou}{with me}, should be labeled with the error tag \tg{wbdPreJoined} and linked with each of the T1 words \e{se} and \e{mnou}.
But because it was only linked to the word \e{mnou}, the automatic annotation incorrectly assigns the error tag \tg{formPre1}.

Word-initial errors where the difference cannot be classified as a common Czech prefix are labeled with \tg{formHead} tags.
The tag \tg{formHead0} is used for missing word-initial characters (\eetc{busovou}{autobusovou}{bus}{adj}),
\tg{formHead} is used for different word beginnings (\eet{prověděl}{dozvěděl}{learned}, \eetc{chiny}{Číny}{China}{gen}) and
\tg{formHead1} for extra characters.
However, the last situation is always a result of errors in manual annotation:
incorrectly fused words were properly corrected (\eet{conejlíp}{co nejlíp}{as good as possible}) but instead of splitting the original T0 word into two (or more) T1 words, linked to the source and labeled together with the \tg{wbdOtherJoined} tag, one of the T1 words was labeled as a correction of the original and the other word was inserted as a missing word.

The opposite case, when the original and corrected words start in the same way but end differently, is labeled with \tg{formTail} tags (\tg{formTail0}, \tg{formTail1}, \tg{formTail}). The \tg{formTail0} tag is used when the original word is missing some characters at the end (\eet{t}{tam}{there}, \eet{ž}{žít}{live};
there are only few meaningful examples in the corpus),
\tg{formTail} is used for words with different ends (\eetc{několiku}{několika}{several}{gen}, \eetc{šansu}{šanci}{chance}{acc}),
\tg{formTail1} for extra characters (no meaningful examples).

Even less information is contained in the \tg{formLen} tags, which are used for words that
(1) differ significantly (but that still do not cross the threshold when we give up on marking differences), and
(2) that they also differ in length.
The tag \tg{formLen0} is used when the original word is shorter than the corrected word (\eet{ňákem}{nějakém}{some}, \eet{diš}{když}{when}), and
\tg{formLen1} when the original word is longer (\eet{vidňanami}{Vídeňany}{Viennese}, \eet{recat}{říct}{say}).

Cases when the original T0 word significantly differs from the corrected T1 word are labeled with the \tg{formUnspec} error tag: \eet{omevy}{umývá}{washes}, \eetc{choubů}{hub}{mushrooms}{gen}, \eet{ěšče}{ještě}{still}.
In retrospect, we should not have introduced the tags \tg{formLen}, \tg{formTail} and \tg{formHead}
-- for words where partial differences cannot be easily automatically identified,
it would be better to give up recognition completely and always use the \tg{formUnspec} tag.

\subsubsection{Automatic classification of word-boundary errors}

\index{error!word boundary}

Word-boundary errors include words either incorrectly fused (\et{semsi}{sem si} `\catt{aux}\cat{1sg} \catt{refl}\cat{dat}') or split (\eet{ne chodila}{nechodila}{wasn’t going}).
During correction, these errors are manually labeled with \tg{wbd} tags:
\tg{wbdPre} is used for prepositions fused with the following words and separated prefixes,
\tg{wbdOther} is used for other word-boundary errors.
The automatic procedure adds a tag to mark whether the forms were
incorrectly separated (\tg{wbdPreSplit} / \tg{wbdOtherSplit}) or
incorrectly joined (\tg{wbdPreJoined} / \tg{wbdOtherJoined}).

%\jirkab{Update table}
% \tomas{Rozdělit tabulku na dvě stránky.}

% \todo{Ověřit formulace (angličtinu) u popisu chyb.}

% \bara{Na tabulky 5.8 a 5.9 se odkazuje pouze v kap. 9 na str. 170. Doplnila bych i do části 5.2}

\begin{table}[htbp]
\begin{footnotesize}
  \setlength\tabcolsep{2pt}
\begin{tabular}{@{}p{0.12\textwidth}>{\raggedright\arraybackslash}p{0.43\textwidth}>{\raggedright\arraybackslash}p{0.43\textwidth}@{}}
Error type    & Error description & Example \\ \hline \hline
\tg{Cap0}     & capitalization: lower{\arrow}upper case & \e{evropě}{\arrow}\e{Evropě}; \e{štědrý}{\arrow}\e{Štědrý} \\
\tg{Cap1}     & capitalization: upper{\arrow}lower case     & \e{Staré}{\arrow}\e{staré}; \e{Rodině}{\arrow}\e{rodině} \\ \hline
\tg{Je0}      & \e{ě{\arrow}je}             & \e{ubjehlo}{\arrow}\e{uběhlo}; \e{Nejvjetší}{\arrow}\e{Největší} \\
\tg{Je1}      & \e{je{\arrow}ě}            & \e{vjeděl}{\arrow}\e{věděl}; \e{vjeci}{\arrow}\e{věci} \\
\tg{Mne0}     & \e{mě{\arrow}mně} & \e{zapoměla}{\arrow}\e{zapomněla} \\%; nejvýznamějších{\arrow}nejvýznamnějších \\
\tg{Mne1}     & \e{mně,mňe,mňě {\arrow} mě} & \e{mněla}{\arrow}\e{měla}; \e{rozumněli}{\arrow}\e{rozuměli} \\ \hline
\tg{Dtn}     & \e{ďe,ťe,ňe}; \e{ďi,ťi,ňi} {\arrow} \e{dě,tě,ně}; \e{di,ti,ni}:  & \e{kuchyňe}{\arrow}\e{kuchyně}; \e{vyměňit}{\arrow}\e{vyměnit} \\ \hline
\tg{DiaU}     & diacritics: \e{ú$\leftrightarrow$ů} & \e{nemúžeš}{\arrow}\e{nemůžeš}; \e{ůkoly}{\arrow}\e{úkoly}\\ \hline
\tg{EpentJ0}	& missing \e{j} between vowels	& \e{pieme}{\arrow}\e{pijeme}; \e{žiou}{\arrow}\e{žijou}\\
\tg{EpentJ1}	& superfluous \e{j} between vowels	& \e{fotografijemi}{\arrow}\e{fotografiemi}; \e{dijamant}{\arrow}\e{diamant}\\ \hline \hline
\tg{Y0}	& \e{i{\arrow}y}; \e{í{\arrow}ý}	& \e{kdiž}{\arrow}\e{když}; \e{svími}{\arrow}\e{svými}\\
\tg{Y1}	& \e{y{\arrow}i}; \e{ý{\arrow}í}	& \e{hystorek}{\arrow}\e{historek}; \e{ostatným}{\arrow}\e{ostatním}\\ \hline
\tg{ProtJ0}   & protethic \e{j}: missing \e{j}        & \e{sem}{\arrow}\e{jsem}; \e{menoval}{\arrow}\e{jmenoval} \\
\tg{ProtJ1}   & protethic \e{j}: extra \e{j}          & \e{jse}{\arrow}\e{se}; \e{jmé}{\arrow}\e{mé} \\ \hline
\tg{Vcd0}     & voicing assimilation: incor.~voiced   & \e{stratíme}{\arrow}\e{ztratíme}; \e{nabítku}{\arrow}\e{nabídku} \\
\tg{Vcd1}     & voicing assimilation: incor.~vcless   & \e{zbalit}{\arrow}\e{sbalit}; \e{nigdo}{\arrow}\e{nikdo}  \\
\tg{VcdFin0}  & word-final voicing: incor.~voiceless  & \e{kdyš}{\arrow}\e{když}; \e{vztach}{\arrow}\e{vztah} \\
\tg{VcdFin1}  & word-final voicing: incor.~voiced     & \e{přez}{\arrow}\e{přes}; \e{pag}{\arrow}\e{pak}   \\
\tg{Vcd}      & voicing: other errors                 & \e{protoše}{\arrow}\e{protože}; \e{hodili}{\arrow}\e{chodili} \\ \hline
\tg{YJ0}      & \e{y{\arrow}j}	& \e{yá}{\arrow}\e{já}; \e{žiyu}{\arrow}\e{žiju} \\
\tg{CK0}      & \e{c{\arrow}k}	& \e{clientovi}{\arrow}\e{klientovi}; \e{culturu}{\arrow}\e{kulturu} \\ \hline
\tg{Gemin0}	& incor.\ single char.\ instead of double	& \e{povinost}{\arrow}\e{povinnost}; \e{polostrov}{\arrow}\e{poloostrov}; \\
\tg{Gemin1}	& incor.\ double char.\ instead of single	& \e{sobbota}{\arrow}\e{sobota}; \e{rukoppis}{\arrow}\e{rukopis}; \\ \hline \hline
\tg{Quant0}     & diacritics: missing vowel accent      & \e{vzpominám}{\arrow}\e{vzpomínám}; \e{doufam}{\arrow}\e{doufám}\\
\tg{Quant1}     & diacritics: extra vowel accent        & \e{ktérá}{\arrow}\e{která}; \e{hledát}{\arrow}\e{hledat} \\ \hline
\tg{Caron0}     & diacritics: missing caron             & \e{vecí}{\arrow}\e{věcí}; \e{sobe}{\arrow}\e{sobě} \\
\tg{Caron1}     & diacritics: extra caron           & \e{břečel}{\arrow}\e{brečel}; \e{bratřem}{\arrow}\e{bratrem} \\ \hline
\tg{DiaE}       & diacritics: \e{ě{\arrow}é}, or \e{é{\arrow}ě}       & \e{usmévavé}{\arrow}\e{usměvavé}; \e{poprvě}{\arrow}\e{poprvé} \\

\tg{Palat0}   & missing palatalization of \e{k,g,h,ch} & \e{amerikě}{\arrow}\e{Americe}; \e{matkě}{\arrow}\e{matce} \\
\tg{Palat1}   & incor. palatalization of \e{k,g,h,ch} & \e{koníčcem}{\arrow}\e{koníčkem}; \e{pracovnícem}{\arrow}\e{pracovníkem} \\ \hline
\tg{EpentE0}  & \e{e} epenthesis: missing \e{e}       & \e{domček}{\arrow}\e{domeček}; \e{najdnou}{\arrow}\e{najednou}\\
\tg{EpentE1}  & \e{e} epenthesis: extra \e{e}         & \e{rozeběhl}{\arrow}\e{rozběhl}; \e{účety}{\arrow}\e{účty} \\ \hline
\tg{ProtV1}   & protethic \e{v}: extra \e{v}          & \e{vosm}{\arrow}\e{osm}; \e{vopravdu}{\arrow}\e{opravdu} \\ \hline
\tg{GH0}	& switch error \e{g{\arrow}h} & \e{glavní}{\arrow}\e{hlavní}; \e{mnogo}{\arrow}\e{mnoho} \\
\tg{GH1}	& switch error \e{h{\arrow}g}& \e{ciharetu}{\arrow}\e{cigaretu}; \e{hrafička}{\arrow}\e{grafička} \\ 

\end{tabular}
\end{footnotesize}
\caption{Formal errors on T1 -- part 1 of 2. All the error tags are prefixed with the string \tg{form}, \eg \tg{formCap0}. We omit this prefix for space reasons.}
\label{tbl:error:formal:T1}
\end{table}

\begin{table}[htbp]
\begin{footnotesize}
  \setlength\tabcolsep{2pt}
\begin{tabular}{@{}p{0.12\textwidth}>{\raggedright\arraybackslash}p{0.43\textwidth}>{\raggedright\arraybackslash}p{0.43\textwidth}@{}}
Error type    & Error description & Example \\ \hline \hline
\tg{Meta}       & character metathesis                           & \e{dobrodružtsví}{\arrow}\e{dobrodružství}; \e{provůdce}{\arrow}\e{průvodce} \\ \hline
\tg{SingCh}     & other erroneous character substitution               & \e{otevřila}{\arrow}\e{otevřela}; \e{vezmíme}{\arrow}\e{vezmeme} \\ \hline
\tg{MissChar}   & other missing character 	                    & \e{protže}{\arrow}\e{protože}; \e{oňostroj}{\arrow}\e{ohňostroj} \\
\tg{RedunChar}  & other extra character                          & \e{opratrně}{\arrow}\e{opatrně}; \e{zrdcátko}{\arrow}\e{zrcátko} \\ \hline \hline

\tg{Pre0} & missing prefix & \e{hledu}{\arrow}\e{pohledu}; \e{žaduje}{\arrow}\e{vyžaduje}; \\

\tg{Pre1}	& superfluous prefix & \e{pojet}{\arrow}\e{jet}; \e{přezačít}{\arrow}\e{začít} \\ \hline

\tg{Head0} & other missing characters word-initial & \e{busovou}{\arrow}\e{autobusovou}; \\
\tg{Head1} & extra characters word-initial & \\
\tg{Head} & different word beginnings & \e{prověděl}{\arrow}\e{dozvěděl}; \e{chiny}{\arrow}\e{Číny}\\ \hline
\tg{Tail0} & missing characters word-final & \e{t}{\arrow}\e{tam}; e{ž}{\arrow}\e{žít}; \\
\tg{Tail11} & extra characters word-final & \\
\tg{Tail} & different word endings & \e{šansu}{\arrow}\e{šanci}; \e{nezajína}{\arrow}\e{nezajímá}\\ \hline
\tg{Len0} & orig.\ word shorter \& unspec.\ errors & \e{ňákem}{\arrow}\e{nějakém}; \e{diš}{\arrow}\e{když} \\
\tg{Len1} &  orig.\ word longer \& unspec.\ errors & \e{vidňanami}{\arrow}\e{Vídeňany} \\ \hline \hline
\tg{Unspec} & too many differences & \e{kreṙénu}{\arrow}\e{kterému}; \e{choubů}{\arrow}\e{hub}\\ 
\end{tabular}
\end{footnotesize}
\caption{Formal errors on T1 -- part 2 of 2. All the error tags are prefixed with the string \tg{form}, omitted here for space reasons.}
\label{tbl:error:formal:T1b}
\end{table}



%\begin{tabular}{@{}lll@{}}
%Error type    & Error description & Example \\ \hline
%\tg{Cap0}     & capitalization: incor.~lower case     & \e{evropě}/\e{Evropě}; %\e{štědrý}/\e{Štědrý} \\
%\tg{Cap1}     & capitalization: incor.~upper case     & \e{Staré}/\e{staré}; \e{Rodině}/\e{rodině} \\ \hline
%\tg{Vcd0}     & voicing assimilation: incor.~voiced   & \e{stratíme}/\e{ztratíme}; %\e{nabítku}/\e{nabídku} \\
%\tg{Vcd1}     & voicing assimilation: incor.~vcless   & \e{zbalit}/\e{sbalit}; %\e{nigdo}/\e{nikdo}  \\
%\tg{VcdFin0}  & word-final voicing: incor.~voiceless  & \e{kdyš}/\e{když}; %\e{vztach}/\e{vztah} \\
%\tg{VcdFin1}  & word-final voicing: incor.~voiced     & \e{přez}/\e{přes}; %\e{pag}/\e{pak}   \\
%\tg{Vcd}      & voicing: other errors                 & \e{protoše}/\e{protože}; %\e{hodili}/\e{chodili} \\ \hline
%\tg{Palat0}   & missing palatalization (\e{k,g,h,ch}) & \e{amerikě}/\e{Americe}; %\e{matkě}/\e{matce} \\
%\tg{Je0}      & \e{je/ě}: incorrect \e{ě}             & \e{ubjehlo}/\e{uběhlo}; %\e{Nejvjetší}/\e{Největší} \\
%\tg{Je1}      & \e{je/ě}: incorrect \e{je}            & \e{vjeděl}/\e{věděl}; %\e{vjeci}/\e{věci} \\
%\tg{Mne0}     & \e{mě/mně}: incorrect \e{mě}          & \e{zapoměla}/\e{zapomněla} %\\%; nejvýznamějších/nejvýznamnějších \\
%\tg{Mne1}     & \e{mě/mně}: incor.~\e{mně, mňe, mňě}  & \e{mněla}/\e{měla}; %\e{rozumněli}/\e{rozuměli} \\ \hline
%\tg{ProtJ0}   & protethic \e{j}: missing \e{j}        & \e{sem}/\e{jsem}; %\e{menoval}/\e{jmenoval} \\
%\tg{ProtJ1}   & protethic \e{j}: extra \e{j}          & \e{jse}/\e{se}; %\e{jmé}/\e{mé} \\
%\tg{ProtV1}   & protethic \e{v}: extra \e{v}          & \e{vosm}/\e{osm}; %\e{vopravdu}/\e{opravdu} \\
%\tg{EpentE0}  & \e{e} epenthesis: missing \e{e}       & \e{domček}/\e{domeček} \\ %; %najdnou/najednou \\
%\tg{EpentE1}  & \e{e} epenthesis: extra \e{e}         & \e{rozeběhl}/\e{rozběhl}; %\e{účety}/\e{účty} \\
%\end{tabular}
%\begin{tabular}{@{}lll@{}}
% Error type     & Error description & Example \\\hline
% \tg{Caron0}     & diacritics: missing wedge             & \emph{vecí}/\emph{věcí}; \emph{sobe}/\emph{sobě} \\
% \tg{Caron1}     & diacritics: extra wedge               & \emph{břečel}/\emph{brečel}; \emph{bratřem}/\emph{bratrem} \\
% \tg{DiaE}       & diacritics: \e{ě/é}, or \e{é/ě}       & \emph{usmévavé}/\emph{usměvavé}; \emph{poprvě}/\emph{poprvé} \\
% \tg{DiaU}       & diacritics: \e{ú/ů}, or \e{ů/ú}       & \emph{nemúžeš}/\emph{nemůžeš}; \emph{ůkoly}/\emph{úkoly} \\
% \tg{Dtn}        & \e{dě/tě/ně}, \e{di/ti/ni} error      & \emph{ňikdo}/\emph{nikdo}; \emph{ješťerka}/\emph{ještěrka} \\
% \tg{Quant0}     & diacritics: missing vowel accent      & \emph{vzpominám}/\emph{vzpomínám}\\%; doufam/doufám \\
% \tg{Quant1}     & diacritics: extra vowel accent        & \emph{ktérá}/\emph{která}; \emph{hledát}/\emph{hledat} \\ \hline
% \tg{Y0}         & \e{i/y} switch error: incorrect \e{i}          & \emph{pražskích}/\emph{pražských}; \emph{vipije}/\emph{vypije} \\
% \tg{Y1}         & \e{i/y} switch error: incorrect \e{y}          & \emph{hlavným}/\emph{hlavním}; \emph{líbyl}/\emph{líbil} \\
% \tg{YJ0}        & \e{y/j} switch error: incorrect \e{y}          & \emph{yaké}/\emph{jaké}; \emph{yazykem}/\emph{jazykem} \\
% \tg{GH0}        & \e{g/h} switch error                           & \emph{gost}/\emph{host}; \emph{gorký}/\emph{horký} \\
% \tg{CK0}        & \e{c/k} switch error: incorrect \e{c}          & \emph{Atlantic}/\emph{Atlantik} \\
%            & (except palatalization)                        & \\ \hline
% \tg{Palat0}     & missing palatalization (\e{k,g,h,ch})          &
% \emph{amerikě}/\emph{Americe}; \emph{matkě}/\emph{matce} \\
% \tg{EpentJ0}    & missing \e{j} between \e{i} and a vowel        & \emph{napie}/\emph{napije} \\
% \tg{EpentJ1}    & extra   \e{j} between \e{i} and a vowel        & \emph{dijamant}/\emph{diamant} \\
% \tg{Gemin0}     & incorrectly non-doubled letter                 & \emph{polostrově}/\emph{poloostrově} \\
% \tg{Gemin1}     & incorrectly doubled letter                     & \emph{essej}/\emph{esej}; \emph{professor}/\emph{profesor} \\ \hline
% \tg{Meta}       & character metathesis                           & \emph{dobrodružtsví}/\emph{dobrodružství} \\ \hline
% % \\[1.5ex] %; provůdce/průvodce
% % \multicolumn{3}{@{}l}{Other errors, single character} \\
% \tg{MissChar}   & other missing character 	                    & \emph{protže}/\emph{protože}; \emph{oňostroj}/\emph{ohňostroj} \\
% \tg{ExtraChar}  & other extra character                          & \emph{opratrně}/\emph{opatrně}; \emph{zrdcátko}/\emph{zrcátko} \\
% \tg{SingCh}     & erroneous character substitution               &
% \emph{otevřila}/\emph{otevřela}; \emph{vezmíme}/\emph{vezmeme} \\ \hline
% % \\[1.5ex]
% % \multicolumn{3}{@{}l}{Other errors, multiple characters} \\
% \tg{Pre}        & unspecified prefix error            & \emph{poletěla}/\emph{letěla}; \emph{potrávíme}/\emph{trávíme} \\
% \tg{Head}       & unspecified word-initial error      & \emph{rustala}/\emph{zůstala}; \emph{žijna}/\emph{října} \\
%                & (except prefix)                 &\\
% \tg{Tail}       & unspecified word-final error & \emph{holkamá}/\emph{holkami}; \emph{nezajína}/\emph{nezajímá} \\
% \tg{Unspec}     & unspecified mid-word error	 &
% \emph{kreṙénu}/\emph{kterému} \\
% %;provudkyně/průvodkyně \\
%\end{tabular}


