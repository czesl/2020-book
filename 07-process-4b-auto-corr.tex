\section{Automatic correction}
\label{sec:process:tiers:korektor}

\index{target hypothesis!automatic}

% \subsection{Using a proofing tool for learner language}

In this section, we discuss automatic correction as a way to normalize texts for which manual error annotation is not available. 
The results depend on the error type and on the immediate context of the error: the closer to standard Czech, the better chance of success \refS{sec:using:nlp:korektor}. 

One of the options to (partially) automate the correction task is to use a proofing tool -- a spell checker or a grammar checker.
So far, we have tested and applied \tool{Korektor},\footnote{%
	See \url{https://ufal.mff.cuni.cz/korektor} and \citet{Richter:2010,Richter:etal:2012,Ramasamy:etal:2015,Naplava:Straka:2019}. 
	There is a win-win cooperation between \tool{Korektor} and \czesl: 
	the tool uses hand-annotated \czesl texts as training data, see \refs{sec:using:nlp:korektor}.} 
a spell checker that has some functionalities of a grammar checker, using a combination of lexicon, morphology and a syntax model.\footnote{%
	\citet{Flor:Futagi:2012} report similar results for \tool{ConSpel}, a tool used to detect and correct non-word misspellings in English, using n-gram statistics based on the \corp{Google Web1T} database.} 

\tool{Korektor} does not provide successive or domain-specific suggestions to match any of the annotation schemes used for the \czesl texts (2T, MD or implicit). Specifically, its output does not match the successive corrections on the two tiers of the 2T annotation scheme. Although the tool does provide an $n$-best option to generate several suggestions ordered according to their assumed plausibility, in practice we always use a single suggestion for each error, \ie the \emph{autocorrect} mode. As long as the source form is a non-word, the suggestion often fits the definition of T1, without any successive correction (\eg lexical) to fit T2. However, the tool can also correct real-word errors which are due to incorrect agreement and other morphosyntactic reasons, thus bypassing T1. 

This is how the \corp{CzeSL-SGT} corpus \refS{sec:data:sgt} was annotated. All non-native texts transcribed in the first round (2009--2012), including those error-annotated manually in the 2T scheme, were corrected using a single TH proposed by the tool and released with linguistic annotation. Automatic correction using the tool as a web service is also available in \tool{TEITOK} as a substitute for manual annotation or to provide suggestions for the annotator.

% The results seem to justify the option to integrate the spell checker
% into the annotation workflow, even though its suggestions may not
% quite match the two distinct tiers without tuning to the specific task
% and annotation scheme.

% \jirka{(1) Tohle presunout na zacatek sekce, (2) v data se pise, ze plain je uplne plain:} \sasa{plain je opravdu úplně plain, Korektor se použil jen na texty cizinců z czesl-plain a vzniklo z toho czesl-sgt. Zatím jsem jic nepřesunul.}

The tool is concerned mainly with errors in orthography and morphemics, and
handles some errors in morphosyntax, including real-word errors, as
long as they are detectable locally, within a reasonably small window
of \textit{n}-grams. Corrections are limited
to single words, targeting a single character or a very small number
of characters by insertion, omission, substitution,
transposition, addition, deletion or substitution of a
diacritic. Errors that involve joining or splitting of word tokens or
word-order errors of any type are not handled at the moment.

If a form detected as incorrect does not correspond to any Czech word (\ie is a non-word), \tool{Korektor} decides it is a spelling error. If it does correspond to a Czech word but is incorrect in the context (due to errors that produce a word which seems to be correct out of context), \tool{Korektor} decides it is an error in grammar (\ie a real-word error). 
  
% We have applied \tool{Korektor}  to all transcribed texts in \czesl, including the texts without any manual annotation \refS{sec:data:sgt}.
  


% The tool was tested on a subset of the 918 ill-formed words in the
% sample, namely on those where the annotators came up with an identical
% emendation – there were 786 such forms, assumed as
% “truth”. \emph{Korektor} was used in three different scenarios. The
% diacritics assigner mode was right in 552 cases (70.2\%), the
% proofreader mode in 639 cases (80.5\%), and the diacritics assigner
% followed by proofreader in 644 cases (81.9\%). Even though T1 data are
% not the optimal benchmark, this tool seems to be useful as an aid to
% the annotator, or even as a way to obtain large quantities of
% annotated (emended) texts at the cost of a higher error rate.

% \subsection{Evaluation of automatic correction}
% \label{sec:process:tiers:korektor:eval}

The performance of \tool{Korektor} was evaluated first by
\citet{Stindlova:2011b} with about 20\% error rate on the set of
non-words, and later by \citet{Ramasamy:etal:2015} on a larger sample. Form errors (resulting in non-words) were
detected with a success rate of 89\%. For grammar errors (real-word
errors) the detection rate was much lower, about 15.5\%. The detection
of accumulated errors (a non-word corrected into a real word with a successive corection) was similar to form errors (89\%). \citet{Naplava:Straka:2019} achieve even better results with a new system, which has not been used to annotate a released \czesl corpus yet. For more about \tool{Korektor} and its evaluation \refS{sec:using:nlp:korektor}.

% \sasa{\citet{Ramasamy:etal:2015} \url{http://ceur-ws.org/Vol-1422/73.pdf} má mnohem větší test set: 33 th. tokens on T1 and 33 th. tokens on T2. Je toho škoda, ale navrhuju zestručnit to, co tady je \citep{Stindlova:2011b}, a trochu rozkecat \citet{Ramasamy:etal:2015} -- o kus dál. Něco je taky v \refs{sec:using:nlp:korektor}, asi přesunout sem a spojit.}

% In \citet{Stindlova:2011b}, the tool was tested on a subset of the pilot set of annotated texts \refS{sec:data},
% produced by learners at intermediate or higher levels of proficiency, yet among the total 9,372 tokens
% (7,995 tokens excluding punctuation) 918 (10\%) were not recognised by
% the morphological analyser included in a Czech POS tagger (see
% \tool{Morče} in \citealp{spoustova:etal:07}). Even more forms were
% judged as faulty by the annotators: 1,189 (13\%) were corrected in the
% same way by both annotators at T1 and 1,519 (16\%) at T2.

% Results of the spell checker were compared with those of the
% morphological analyzer and with forms at T1 and T2, provided
% both annotators were in agreement. The spell checker was run in three
% (batch) modes:
% (i) ``autocorrect'' (as proofreader),
% (ii) ``remove-diacritics'' followed by ``diacritics'' (as diacritics assigner), and
% (iii) same as in (ii), followed by ``autocorrect'';
% the latter two to test the hypothesis that diacritics is a frequent sourceof errors.

% Although the morphological analyzer includes a guesser, it makes no
% attempt to correct an unknown word form, only guesses its
% morphosyntactic tag and lemma. The spell checker is deemed to be
% successful for a given form if the morphological analyser treats it as unknown
% and the spell checker suggests a correction, or if the analyser treats
% the form as known and the spell checker leaves it intact.

% \autoref{tbl:process:auto-correct:morce} shows figures for the morphological analyser.
% The rows give results for the three modes:
% \emph{autocorrect} (i),
% \emph{diacritics} for ``remove-diacritics'' followed by ``diacritics'' (ii), and
% \emph{autocorrect} + \emph{diacritics} for the full sequence (iii).
% The column ``corrected'' gives the counts for forms corrected by the spell checker run in the relevant mode.
% The column ``unknown'' gives the number of cases where the morphological analyser happens to flag a form corrected by the spell checker as unknown.
% The results of the analyser are assumed as truth for the
% purpose of calculating precision (``unknown''/``corrected'') and
% recall (``unknown''/918, the latter figure representing all forms unknown to analyser).

% Precision is not really a fair measure here, because the analyser
% never flags forms which are correct in isolation but faulty in a
% context, while the spell checker often manages to use local context to
% replace a form X with an orthographically close but
% morphosyntactically quite different for Y:
% \e{podlé $\rightarrow$ podle, jejích $\rightarrow$ jejich,
%   žit $\rightarrow$ žít, libí $\rightarrow$ líbí, ze $\rightarrow$ že,
%   divá $\rightarrow$ dívá, drahy $\rightarrow$ drahý, mel
%   $\rightarrow$ měl,
%   jích $\rightarrow$ jich, čine $\rightarrow$ číně}. Interestingly,
% diacritics seem to represent a substantial share of problems in
% learners' writings, and the preprocessing of the input by the
% diacritics remover and assigner (iii) means a significant improvement.

% \begin{table}[h]
% \begin{tabular}{lrrrrr}
% \tblline
%  mode & corrected & unknown & precision & recall & F-measure \\
% \noalign{\smallskip}\tblline
% \emph{autocorrect} & 1151 & 888 & 0.77 & 0.97 & 0.86 \\
% \emph{diacritics} & 1176 & 795 & 0.68 & 0.87 & 0.76 \\
% \emph{autocorrect} + \emph{diacritics} & 1315 & 906 & 0.69 & 0.99 & 0.81 \\
% \noalign{\smallskip}\hline
% \end{tabular}
% \caption{Comparison with morphological analyser, which identified the total of 918 unknown forms}
% \label{tbl:process:auto-correct:morce}
% \end{table}

% Corrections made by the annotators can be compared verbatim with those
% proposed by the spell checker. The spell checker scores whenever the
% form proposed by the relevant mode matches the form at T1 or
% T2, respectively. The two annotators must agree about the
% corrected form, only then it is seen as fit for comparison.	

% % \comai{This text is hard to understand} \comsi{Better?}
% At T1 the total number of corrections (1189) is higher than the
% number of forms unknown to the morphological analyser (918) because
% the annotators correct also misspellings which look like homographs
% with an existing form. Such faulty forms are never detected by the
% morphological analyser. As a result, recall of the spell checker is
% lower when its performance is compared with T1 than when with the
% results of the morphological analyser. Precision stays
% roughly the same as in the previous comparison because in one aspect
% T1 is similar to the analyser: it still largely abstracts from
% context. \eg annotators are instructed to leave errors due to missed
% grammatical concord for T2.
% The data are shown in \autoref{tbl:process:auto-correct:l1} --
% the column ``corrected'' is identical to that in \autoref{tbl:process:auto-correct:morce},
% but the ``wrong'' column shows the number of cases where the two annotators agree about an emended form, identical to the suggestion of the spell checker.


% \begin{table}[h]
% \begin{tabular}{lrrrrr}
% \tblline
% mode & corrected & wrong & precision & recall & F-measure \\
% \noalign{\smallskip}\tblline
% \emph{autocorrect} & 1151 & 846 & 0.74 & 0.71 & 0.72 \\
% \emph{diacritics} & 1176 & 780 & 0.66 & 0.66 & 0.66 \\
% \emph{autocorrect} + \emph{diacritics} & 1315 & 904 & 0.69 & 0.76 & 0.72 \\
% \noalign{\smallskip}\hline
% \end{tabular}
% \caption{Comparison with corrections at T1, where annotators agreed on the total of 1189 wrong forms}
% \label{tbl:process:auto-correct:l1}
% \end{table}

% It is interesting to investigate cases where the spell checker does not agree with the annotators,
% but both the spell checker and the annotators indicate an error (170 such cases at T1 for the \emph{autocorrect} + \emph{diacritics} mode).
% In some of these cases, the simple \emph{autocorrect} mode without the diacritics component fares better (in 30 cases out of 170).
% It seems that removing and reassigning diacritics takes the spell-checker too
% far (\autoref{tbl:process:auto-correct:cor}).
% In some cases, the T1 and T2 versions differ and none of the methods matches the contextually correct version of T2 (\ee{pláž}{beach}, \ee{lépe}{better}).

% \begin{table}[h]
% \begin{tabular}{lllll}
% \tblline
% T0 & \emph{autocorrect}+\emph{diacritics} & T1=\emph{autocorrect} & T2 & T2 gloss\\
% \noalign{\smallskip}\tblline
% \e{plaži} & \e{pláží} & \e{pláži} & \e{pláž} & `beach' \\
%  \e{tydnů} & \e{týdnů} & \e{týdnu} & \e{týdnu} & `week.\cat{dat/loc}'\\
%  \e{lepšé} & \e{lepše} & \e{lepší} & \e{lépe} & `better'\\
%  \e{jide} & \e{lidé} & \e{jde} & \e{jde} & `goes' \\
%  \e{vždicky} & \e{vodičky} & \e{vždycky} & \e{vždycky} & `always'\\
% %	 \e{všihny} & \e{viny} & \e{všichni} & \e{všichni} & `all'\\
% %	 \e{zmřel} & \e{zmrzl} & \e{zemřel} & \e{zemřel} & `died\sub{masc}'\\
% %	 \e{sejdime} & \e{šmejdíme} & \e{sejdeme} & \e{sejdeme} & `meet\sub{1pl}'\\
% \noalign{\smallskip}\hline
% \end{tabular}
% \caption{Where simple autocorrect mode is better}
% \label{tbl:process:auto-correct:cor}
% \end{table}

% In 150 cases, the spell checker suggests a correction when T1 prefers the original,
% but in 37 cases, the spell checker agrees with an annotator at T2 (in 16 cases with both), which means that the real precision is higher.
% The rest of the cases are mostly inflectional issues, often due to misassigned diacritics, but also quite a few errors in the annotation (shared by both annotators).

% T2 is problematic for evaluation in its own right.
% Some error types handled here are due to wrong word order,
% style, phraseology and a few other that go beyond simple spell
% checking, even in a broader sense of some degree of contextual sensitivity.
% The figures in \autoref{tbl:process:auto-correct:l2}, otherwise similar to \autoref{tbl:process:auto-correct:l1}, should be interpreted accordingly.
						
% \begin{table}[h]
% \begin{tabular}{lrrrrr}
% \tblline						
% mode & corrected & wrong & precision & recall & F-measure \\
% \noalign{\smallskip}\tblline
% \emph{autocorrect} & 1151 & 687 & 0.60 & 0.45 & 0.51 \\
% \emph{diacritics} & 1176 & 640 & 0.54 & 0.42 & 0.47 \\
% \emph{autocorrect} + \emph{diacritics} & 1315 & 745 & 0.57 & 0.49 & 0.53 \\
% \noalign{\smallskip}\hline
% \end{tabular}
% \caption{Comparison with corrections at T2, where annotators agreed on the total of 1519 wrong forms}
% \label{tbl:process:auto-correct:l2}
% \end{table}


% %%%%%%%%%
% \textbf{\citet{Ramasamy:etal:2015}}: In an optimal
% setting of the model, the best results achieved in terms of F1 score \bara{je někde vysvětleno?}
% were 95.4\% for error detection and 91.0\% for error correction. In a
% manual analysis of 3000 tokens, about 23\% of the tokens included
% either a form error at Tier~1 (62\%), a grammar error at Tier~2
% (27\%), or an accumulated error at both tiers (11\%). Form errors were
% detected with a success rate of 89\%. For grammar errors (real-word
% errors) the detection rate was much lower, about 15.5\%. The detection
% of accumulated errors was similar to form errors (89\%).
% %%%%%%%%%



% The two-stage annotation scheme suggests the option to distinguish
% corrections of forms that are wrong in any context, from those that
% could be correct in isolation, or in a different context, \ie to test
% the grammar-checking capabilities of the spell checker. However,
% \tool{Korektor} does not quite match the annotation scheme. It is only
% possible to find a few individual cases of successful corrections of
% missed agreement or case government (in the order of tens). Again, as
% in all the previous cases, the mode combining diacritics remover,
% assigner and proofreader is the best scenario.

% The results justify the option to integrate the spell checker
% into the annotation workflow, even though its suggestions may not
% quite match the two distinct tiers without tuning to the specific task
% and annotation scheme.


