\section{Implicit annotation}
\label{sec:process:implicit}

\index{error!implicit categorization}

Manual error annotation can be easier when one of the two parts of the error annotation task is omitted (correction or categorization). In both of our two attempts to simplify error annotation this way we applied error correction, omitting categorization.\footnote{See \refs{sec:error:implicit} for more about implicit error annotation.}

The first approach is based on the previous experience with the 2T error annotation scheme. We used the same toolchain, including \tool{feat} as the annotation editor, and the same annotation guidelines, including the distinction of T1 and T2 and the geometry of cross-tier links for splitting, joining and reordering tokens. However, no error tags were used.

In 2017, 1,300 texts (180 thousand tokens) were manually corrected. The texts were selected from the pool of texts annotated only by automatic tools in \textit{CzeSL-SGT} to partially fill the under-represented groups of learners according to the combined L1 and CEFR specifications. The annotated texts are released as \corp{CzeSL-TH} \refS{sec:data:th} and as a part of the \textit{AKCES-GEC} dataset \refS{sec:data:gec}.

We have also tested and adopted an approach based on a sequence of target hypotheses corresponding to linguistic notions such as spelling, morphology, syntax or lexicon with a radically simplified error categorization part, using \tool{TEITOK} as the annotation tool.\footnote{See \refs{sec:tools:teitok} for more about the tool. Several learner and historical corpora are available in \textit{TEITOK}, with annotation based mainly on corrections.}

The first major application of this type of annotation was in a corpus of native learners of Czech -- \corp{SKRIPT 2015} \refS{sec:data:native}. The corpus is based on already existing transcripts. A part of the corpus overlaps with \corp{SKRIPT 2012}, which was released without linguistic or error annotation. The texts were converted from transcripts using the original transcription markup into XML and, if necessary, manually anonymized. Then the texts were hand-corrected at four levels: (i) rectification of non-standard forms (resulting in a form that is still non-standard but spelled ``correctly''), (ii) spelling and morphonology (correcting even ``correctly spelled'' non-standard forms), (iii) morphosyntax and (iv) lexicon. Most of the levels were tagged and lemmatized, and annotated with the formal error tags \refS{sec:error:tiers:formal}.

Due to a positive experience with this fairly large-scale manual annotation project, other \czesl texts without manual annotation included in \corp{CzeSL-SGT} are due to be annotated in the same way, while the already existing manually annotated parts of \textit{CzeSL} will be integrated into the result -- \corp{CzeSL in TEITOK}. Importantly, the annotation in \corp{CzeSL in TEITOK} is compatible with the 2T annotation scheme. 

Based on experience and options, the following data can be used in a corpus built in \tool{TEITOK}: 

\begin{description}
\item[New texts] (manuscripts or audio) can be  transcribed and anonymized in \tool{TEITOK} in the XML format
\item[Existing transcripts] in the old format, possibly anonymized, can be converted into the \tool{TEITOK} XML format using a conversion tool \refS{sec:tools:conv}
\item[2T error-annotated texts] -- including texts without error tags, can be converted into the \tool{TEITOK} XML format; some annotation can be expressed inline, other annotation (more complex cross-tier links) in a stand-off annotation format
\item[MD error-annotation] can be added to the \tool{TEITOK} XML in the stand-off format
\end{description}

Once the data are included in a \tool{TEITOK} corpus, they can be annotated in the following ways:

\begin{description}
\item[Error annotation] ~
  \begin{description}
  \item[automatic:] TH guessing (\tool{Korektor} web service\footnote{\url{https://lindat.mff.cuni.cz/services/korektor/api-reference.php}}),
    formal error tags
  \item[manual:] successive corrections, implicitly specifying the error type (corresponding to 2T tiers and some 2T error tags or to MD error domains)
  \end{description}
\item[Linguistic annotation] ~
  \begin{description}
  \item[automatic:] lemmas and tags for the source and/or any
    correction level (\tool{MorphoDiTa} web service\footnote{\url{https://lindat.mff.cuni.cz/services/morphodita/api-reference.php}}), syntactic structure
    (\tool{UDPipe} web service\footnote{\url{https://lindat.mff.cuni.cz/services/udpipe/api-reference.php}})
  \item[manual:] checking and editing
  \end{description}
\end{description}
